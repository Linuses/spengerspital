package at.spg.repositories;

import at.spg.model.PractitionerRole;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PracRoleRepository extends PagingAndSortingRepository<PractitionerRole,String> {
}