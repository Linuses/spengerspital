package at.spg.repositories;

import at.spg.model.Practitioner;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PractitionerRepository extends PagingAndSortingRepository<Practitioner, String>{

}
