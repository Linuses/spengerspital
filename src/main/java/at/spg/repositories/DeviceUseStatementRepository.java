package at.spg.repositories;

import at.spg.model.DeviceUseStatement;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DeviceUseStatementRepository extends PagingAndSortingRepository<DeviceUseStatement,String> {
}
