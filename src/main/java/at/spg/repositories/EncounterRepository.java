package at.spg.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import at.spg.model.Encounter;

public interface EncounterRepository extends PagingAndSortingRepository<Encounter, String>{

}