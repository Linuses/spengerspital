package at.spg.repositories;

import at.spg.model.Practitioner;
import at.spg.model.ResearchSubject;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ResearchSubjectRepository extends PagingAndSortingRepository<ResearchSubject, String>{

}
