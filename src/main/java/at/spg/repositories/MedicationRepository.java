package at.spg.repositories;

import at.spg.model.Medication;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MedicationRepository extends PagingAndSortingRepository<Medication,String> {
}
