package at.spg.repositories;

import at.spg.model.DeviceUseStatement;
import at.spg.model.Flag;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FlagRepository extends PagingAndSortingRepository<Flag,String> {
}
