package at.spg.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

import at.spg.model.Greeting;

public interface GreetingRepository extends PagingAndSortingRepository<Greeting, Long>{
	
	 List<Greeting> findById(@Param("id") long id); //damit kann man Pseudo Selects noch machen
	 List<Greeting> findByContent(@Param("content") String content); 
}