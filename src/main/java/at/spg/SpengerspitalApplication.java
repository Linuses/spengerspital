package at.spg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EntityScan(basePackageClasses = { SpengerspitalApplication.class, Jsr310JpaConverters.class })
//https://mkyong.com/spring-boot/spring-boot-spring-data-jpa-java-8-date-and-time-jsr310/
public class SpengerspitalApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpengerspitalApplication.class, args);
	}
}