package at.spg.model;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class
Flag extends DomainResource{
    public Flag(Set<Identifier> identifier, StatusCode status, Reference other, Period period) {
        this.identifier = identifier;
        this.status = status;
        this.other = other;
        this.period = period;
    }

    public Flag() { super();
    }

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
    @JoinColumn(name = "i_flag_fk", referencedColumnName = "id")
    private Set<Identifier> identifier = new HashSet<>();


    public enum StatusCode {
        all("all"),
        between("between"),
        neverdothat("never-do-that");

        private String value;
        private StatusCode(String value)
        {this.value = value;}

        public String toString()
        {return this.value; }
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name="flag_status")
    private Flag.StatusCode status = Flag.StatusCode.all;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL) //CascadeType.ALL muss man bei Entities machen,
        // dass wenn Flag gelöscht wird, auch other gelöscht wird, oder wenn man was updatet auch was geupdatet wird
    @JoinColumn(name = "flag_reference")
    private Reference other;

    @Embedded
    private Period period;

    public Set<Identifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Set<Identifier> identifier) {
        this.identifier = identifier;
    }

    public StatusCode getStatus() {
        return status;
    }

    public void setStatus(StatusCode status) {
        this.status = status;
    }

    public Reference getOther() {
        return other;
    }

    public void setOther(Reference other) {
        this.other = other;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }
}
