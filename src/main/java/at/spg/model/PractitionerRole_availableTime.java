package at.spg.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Entity
public class PractitionerRole_availableTime extends BackboneElement{

    public PractitionerRole_availableTime(Set<PractitionerRole_availableTime.DaysOfWeek> daysOfWeek, boolean allDay, LocalDateTime availableStartTime, LocalDateTime availableEndTime) {
        this.daysOfWeek = daysOfWeek;
        this.allDay = allDay;
        this.availableStartTime = availableStartTime;
        this.availableEndTime = availableEndTime;
    }

    public PractitionerRole_availableTime(Set<Extension> modifierExtension, Set<PractitionerRole_availableTime.DaysOfWeek> daysOfWeek, boolean allDay, LocalDateTime availableStartTime, LocalDateTime availableEndTime) {
        super(modifierExtension);
        this.daysOfWeek = daysOfWeek;
        this.allDay = allDay;
        this.availableStartTime = availableStartTime;
        this.availableEndTime = availableEndTime;
    }

    public PractitionerRole_availableTime() {super();}

    public enum DaysOfWeek {
        mon,tue,wed,thu,fri,sat,sun;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name= "daysOfWeek", joinColumns = @JoinColumn(name="id"))
    @Column(name = "pracRoleAt_daysOfWeek")
    @Enumerated(EnumType.STRING)
    private Set<PractitionerRole_availableTime.DaysOfWeek> daysOfWeek = new HashSet<>();

    @Column(name="pracRoleAt_allDay")
    private boolean allDay;

    @Column(name="pracRoleAt_availableStartTime")
    private LocalDateTime availableStartTime;

    @Column(name="pracRoleAt_availableEndTime")
    private LocalDateTime availableEndTime;

    public Set<PractitionerRole_availableTime.DaysOfWeek> getDaysOfWeek() {
        return daysOfWeek;
    }

    public void setDaysOfWeek(Set<PractitionerRole_availableTime.DaysOfWeek> daysOfWeek) {
        this.daysOfWeek = daysOfWeek;
    }

    public boolean isAllDay() {
        return allDay;
    }

    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }

    public LocalDateTime getAvailableStartTime() {
        return availableStartTime;
    }

    public void setAvailableStartTime(LocalDateTime availableStartTime) {
        this.availableStartTime = availableStartTime;
    }

    public LocalDateTime getAvailableEndTime() {
        return availableEndTime;
    }

    public void setAvailableEndTime(LocalDateTime availableEndTime) {
        this.availableEndTime = availableEndTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PractitionerRole_availableTime that = (PractitionerRole_availableTime) o;
        return allDay == that.allDay && Objects.equals(daysOfWeek, that.daysOfWeek) && Objects.equals(availableStartTime, that.availableStartTime) && Objects.equals(availableEndTime, that.availableEndTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), daysOfWeek, allDay, availableStartTime, availableEndTime);
    }
}
