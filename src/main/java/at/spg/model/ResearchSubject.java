package at.spg.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class ResearchSubject extends DomainResource {

    public ResearchSubject(Identifier identifier, @NotNull StatusCode status, Period period, @NotNull Reference individualization, LocalDateTime rs_datetime) {
        this.identifier = identifier;
        this.status = status;
        this.period = period;
        this.individualization = individualization;
        this.rs_datetime = rs_datetime;
    }

    public ResearchSubject(@NotNull StatusCode status, @NotNull Reference individualization) {
        this.status = status;
        this.individualization = individualization;
    }
    protected ResearchSubject(){super();}

//    public ResearchSubject(){super();}


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rs_identifier")
    private Identifier identifier;

    public enum StatusCode {
        candit("candit"),
        enroll("en-roll"),
        inactive("in-active"),
        suspend("suspend"),
        withdrawn("withdrawn"),
        completed("completed");

        private String value;
        private StatusCode(String value)
        {this.value = value;}

        public String toString()
        {return this.value; }
    }

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name="rs_status")
    private ResearchSubject.StatusCode status = StatusCode.withdrawn;

    @Embedded
    private Period period;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "rs_individualization")
    private Reference individualization;

    @Column(name="rs_datetime")
    private LocalDateTime rs_datetime;

    public Identifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Identifier identifier) {
        this.identifier = identifier;
    }

    public StatusCode getStatus() {
        return status;
    }

    public void setStatus(StatusCode status) {
        this.status = status;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Reference getIndividualization() {
        return individualization;
    }

    public void setIndividualization(Reference individualization) {
        this.individualization = individualization;
    }

    public LocalDateTime getRs_datetime() {
        return rs_datetime;
    }

    public void setRs_datetime(LocalDateTime rs_datetime) {
        this.rs_datetime = rs_datetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ResearchSubject that = (ResearchSubject) o;
        return Objects.equals(identifier, that.identifier) && status == that.status && Objects.equals(period, that.period) && Objects.equals(individualization, that.individualization) && Objects.equals(rs_datetime, that.rs_datetime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), identifier, status, period, individualization, rs_datetime);
    }
}
