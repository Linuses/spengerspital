package at.spg.model;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
public class Diagnosis extends BackboneElement{

    public Diagnosis() {super();}

    public Diagnosis(Reference condition, CodeableConcept use, Integer rank) {
        this.condition = condition;
        this.use = use;
        this.rank = rank;
    }

    public Diagnosis(Set<Extension> modifierExtension, Reference condition, CodeableConcept use, Integer rank) {
        super(modifierExtension);
        this.condition = condition;
        this.use = use;
        this.rank = rank;
    }

    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name= "dia_condition_fk", referencedColumnName = "id")
    private Reference condition;

    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name= "dia_use_fk", referencedColumnName = "id")
    private CodeableConcept use;
//    @Embedded
//    private  CodeableConcept use; //Fehlermeldung: org.hibernate.MappingException: component property not found: id

    @Column(name="diagnosis_rank")
    private Integer rank;

    public Reference getCondition() {
        return condition;
    }

    public void setCondition(Reference condition) {
        this.condition = condition;
    }

    public CodeableConcept getUse() {
        return use;
    }

    public void setUse(CodeableConcept use) {
        this.use = use;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Diagnosis diagnosis = (Diagnosis) o;
        return Objects.equals(condition, diagnosis.condition) &&
                Objects.equals(use, diagnosis.use) &&
                Objects.equals(rank, diagnosis.rank);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), condition, use, rank);
    }
}
