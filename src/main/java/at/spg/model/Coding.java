package at.spg.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Objects;

@Entity
public class Coding extends Element{
	
	public Coding() {
		super();
	};

	public Coding(String system, String version, String display, String code, boolean userSelected) {
		super();
		this.system = system;
		this.version = version;
		this.display = display;
		this.code = code;
		this.userSelected = userSelected;
	}

	@Column(name="c_system")
	private String system;
	@Column(name="c_version")
	private String version;
	@Column(name="c_display")
	private String display;
	@Column(name="c_code")
	private String code;
	@Column(name="c_userSelected")
	private boolean userSelected;

	public String getSystem() {
		return system;
	}

	public void setSystem(String system) {
		this.system = system;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isUserSelected() {
		return userSelected;
	}

	public void setUserSelected(boolean userSelected) {
		this.userSelected = userSelected;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;
		Coding coding = (Coding) o;
		return userSelected == coding.userSelected && Objects.equals(system, coding.system) && Objects.equals(version, coding.version) && Objects.equals(display, coding.display) && Objects.equals(code, coding.code);
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), system, version, display, code, userSelected);
	}
}
