package at.spg.model;

import javax.persistence.*;

import at.spg.model.Identifier.UseCode;
import at.spg.model.Period;

import java.util.HashSet;
import java.util.Set;

@Entity
public class Statushistory extends BackboneElement{

    public Statushistory() {
		super();
    }
    
    public Statushistory(StatusCode status, Period period) {
		super();
		this.status = status;
		this.period = period;
	}

	public enum StatusCode{
        planned , arrived , triaged , inprogress , onleave , finished ,cancelled
    }

    //darf kein null in DB stehen
	//Annotation NotNull hinzufügen
    @Enumerated(EnumType.STRING)
	@Column(name="statusHis_code") //i_ wegen iiiidentifier
	private StatusCode status=StatusCode.onleave;
    
    @Embedded
	private Period period;

	public StatusCode getStatus() {
		return status;
	}

	public void setStatus(StatusCode status) {
		this.status = status;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((period == null) ? 0 : period.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Statushistory other = (Statushistory) obj;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		if (status != other.status)
			return false;
		return true;
	}
    
    
}