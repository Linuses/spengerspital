package at.spg.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

//@MappedSuperclass
@Entity
public class Participant extends BackboneElement{
    //TODO zwischen participant und encounter muss es eine referenz geben

    public Participant() {super();}

    public Participant(Set<CodeableConcept> type, Period period, Reference individual) {
        this.type = type;
        this.period = period;
        this.individual = individual;
    }

    public Participant(Set<Extension> modifierExtension, Set<CodeableConcept> type, Period period, Reference individual) {
        super(modifierExtension);
        this.type = type;
        this.period = period;
        this.individual = individual;
    }

    @OneToMany(cascade = CascadeType.ALL, targetEntity = CodeableConcept.class)
    @JoinColumn(name= "cc_participant_fk", referencedColumnName = "id")
    private Set<CodeableConcept> type= new HashSet<>();

    @Embedded
    private Period period;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name= "participant_individual_fk", referencedColumnName = "id")
    private Reference individual;

    public Set<CodeableConcept> getType() {
        return type;
    }

    public void setType(Set<CodeableConcept> type) {
        this.type = type;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Reference getIndividual() {
        return individual;
    }

    public void setIndividual(Reference individual) {
        this.individual = individual;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Participant that = (Participant) o;
        return Objects.equals(type, that.type) &&
                Objects.equals(period, that.period) &&
                Objects.equals(individual, that.individual);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), type, period, individual);
    }
}
