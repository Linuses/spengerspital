package at.spg.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Entity
public class Batch extends BackboneElement{
    public Batch() {super();}

    public Batch(String lotNumber, LocalDate expirationDate) {
        this.lotNumber = lotNumber;
        this.expirationDate = expirationDate;
    }

    public Batch(Set<Extension> modifierExtension, String lotNumber, LocalDate expirationDate) {
        super(modifierExtension);
        this.lotNumber = lotNumber;
        this.expirationDate = expirationDate;
    }

    @Column(name="batch_lotNumber")
    private String lotNumber;


    @Column(name="batch_expirationDate")
    @Getter
    @Setter
    private LocalDate expirationDate;

    public String getLotNumber() {
        return lotNumber;
    }

    public void setLotNumber(String lotNumber) {
        this.lotNumber = lotNumber;
    }
}
