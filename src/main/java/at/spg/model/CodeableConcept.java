package at.spg.model;

import javax.persistence.*;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;

import java.util.Set;

@Entity
@Embeddable
public class CodeableConcept extends Element{
	
	public CodeableConcept() {
		super(); //die Sachen aus den überen
	}
	
	public CodeableConcept(Set<Coding> coding, String text) {
		super();
		this.coding = coding;
		this.text = text;
	}
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Coding.class)
	@JoinColumn(name= "coding_codeableconcept_fk", referencedColumnName = "id") //c für coding und es referenziert auf codeableconcept
	private Set<Coding> coding;
	
	@Column(name="cc_text")
	private String text;

	public Set<Coding> getCoding() {
		return coding;
	}

	public void setCoding(Set<Coding> coding) {
		this.coding = coding;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((coding == null) ? 0 : coding.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CodeableConcept other = (CodeableConcept) obj;
		if (coding == null) {
			if (other.coding != null)
				return false;
		} else if (!coding.equals(other.coding))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	
	
	
}
