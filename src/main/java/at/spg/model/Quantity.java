package at.spg.model;

import java.net.URI;
import java.net.URL;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;


@Entity
public class Quantity extends Element{
    public Quantity() {super();}

    public Quantity(QuantityComparator quantityComparator, Float value, String unit, String uri, String code) {
        this.quantityComparator = quantityComparator;
        this.value = value;
        this.unit = unit;
        this.uri = uri;
        this.code = code;
    }

    public enum QuantityComparator {
        lessthan("<"),
        lessorequal("<="),
        moreorequal(">="),
        more(">");
        private String value;
        private QuantityComparator(String value)
        { this.value = value; }

        public String toString()
        { return this.value; }
    }


    @Enumerated(EnumType.STRING)
    @Column(name="quant_comperator_fk")
    private Quantity.QuantityComparator quantityComparator = QuantityComparator.lessthan;

    @Column(name="quant_value_fk")
    private Float value;
    @Column(name="quant_unit_fk")
    private String unit;
    @Column(name="quant_uri_fk")
    private String uri;
    @Column(name="quant_code_fk")
    private String code;

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public QuantityComparator getQuantityComparator() {
        return quantityComparator;
    }

    public void setQuantityComparator(QuantityComparator quantityComparator) {
        this.quantityComparator = quantityComparator;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
