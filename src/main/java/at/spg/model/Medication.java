package at.spg.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Set;

//https://hl7.org/FHIR/medication.html
@Entity
@EqualsAndHashCode(callSuper = true)
public @Data class Medication extends DomainResource {
    public Medication() { super(); }

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
    @JoinColumn(name="med_identifier_fk",referencedColumnName = "id")
    private Set<Identifier> identifier;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="med_code_fk", referencedColumnName = "id")
    private CodeableConcept code;

    public Medication(Set<Identifier> identifier, CodeableConcept code,
                      StatusCode status, Reference manufacterer, CodeableConcept form,
                      Ratio amount, Set<Ingredient> ingredients, Batch batch) {
        this.identifier = identifier;
        this.code = code;
        this.status = status;
        this.manufacterer = manufacterer;
        this.form = form;
        this.amount = amount;
        this.ingredients = ingredients;
        this.batch = batch;
    }


    public enum StatusCode {
        active("active"),
        inactive("inactive"),
        enteredinerror("entered-in-error");

        private String value;
        private StatusCode(String value)
        {this.value = value;}

        public String toString()
        {return this.value; }
    }

    @Enumerated(EnumType.STRING)
    @Column(name="med_status_fk")
    private StatusCode status = StatusCode.active;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="med_manufacterer_fk", referencedColumnName = "id")
    private Reference manufacterer;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="med_form_fk", referencedColumnName = "id")
    private CodeableConcept form;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="med_amount_fk", referencedColumnName = "id")
    private Ratio amount;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Ingredient.class)
    @JoinColumn(name="ingredient_med_fk",referencedColumnName = "id")
    private Set<Ingredient> ingredients;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="med_batch_fk", referencedColumnName = "id")
    private Batch batch;
}
