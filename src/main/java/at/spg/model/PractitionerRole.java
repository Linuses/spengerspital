package at.spg.model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class PractitionerRole extends DomainResource {

    public PractitionerRole(Set<Identifier> identifier, boolean active, Period period, Reference practitioner, Set<CodeableConcept> speciality, Set<PractitionerRole_availableTime> availableTime) {
        this.identifier = identifier;
        this.active = active;
        this.period = period;
        this.practitioner = practitioner;
        this.speciality = speciality;
        this.availableTime = availableTime;
    }

    public PractitionerRole() {super();}

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
    @JoinColumn(name = "i_pracRole_fk", referencedColumnName = "id")
    private Set<Identifier> identifier = new HashSet<>();

    @Column(name="pracRole_active")
    private boolean active;

    @Embedded
    private Period period;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pracRole_reference")
    private Reference practitioner;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = CodeableConcept.class)
    @JoinColumn(name = "cc_pracRole_fk", referencedColumnName = "id")
    private Set<CodeableConcept> speciality = new HashSet<>();


    @OneToMany(cascade = CascadeType.ALL, targetEntity = PractitionerRole_availableTime.class)
    @JoinColumn(name = "bbe_pracRole_fk", referencedColumnName = "id")
    private Set<PractitionerRole_availableTime> availableTime = new HashSet<>();

    public Set<Identifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Set<Identifier> identifier) {
        this.identifier = identifier;
    }

    public boolean getActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Reference getPractitioner() {
        return practitioner;
    }

    public void setPractitioner(Reference practitioner) {
        this.practitioner = practitioner;
    }

    public Set<CodeableConcept> getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Set<CodeableConcept> speciality) {
        this.speciality = speciality;
    }

    public Set<PractitionerRole_availableTime> getAvailableTime() {
        return availableTime;
    }

    public void setAvailableTime(Set<PractitionerRole_availableTime> availableTime) {
        this.availableTime = availableTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PractitionerRole that = (PractitionerRole) o;
        return Objects.equals(identifier, that.identifier) && Objects.equals(active, that.active) && Objects.equals(period, that.period) && Objects.equals(practitioner, that.practitioner) && Objects.equals(speciality, that.speciality) && Objects.equals(availableTime, that.availableTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), identifier, active, period, practitioner, speciality, availableTime);
    }
}
