package at.spg.model;

import javax.persistence.*;

@Entity
public class ContactPoint extends Element{
	
	public ContactPoint() {	super();}
	
	
	public ContactPoint(String ctp_value, SystemCode system, UseCode use, int rank, Period period) {
		super();
		this.ctp_value = ctp_value;
		this.system = system;
		this.use = use;
		this.rank = rank;
		this.period = period;
	}
	

	public enum SystemCode {
		phone ,fax , email , pager , url , sms ,other
		//ContactPointSystem (Required)
	}
	
	public enum UseCode {
		home , work , temp , old , mobile // purpose of this contact point
		//ContactPointUse (Required)
	}
	
	@Column(name="ctp_value")
	private String ctp_value;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ctp_system")
	private SystemCode system=SystemCode.other;
	
	@Enumerated(EnumType.STRING)
	@Column(name="ctp_use")
	private UseCode use=UseCode.temp;
	
	@Column(name="ctp_rank")
	private int rank; //bei Patientcontroller muss man dann mit get und post über
	//prüfen ob positiv ist
	
	@JoinColumn(name="ctp_period")
	@Embedded
	private Period period;

	public String getCtp_value() {
		return ctp_value; }

	public void setCtp_value(String ctp_value) {
		this.ctp_value = ctp_value;
	}

	public SystemCode getSystem() {
		return system;
	}

	public void setSystem(SystemCode system) {
		this.system = system;
	}

	public UseCode getUse() {
		return use;
	}

	public void setUse(UseCode use) {
		this.use = use;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((ctp_value == null) ? 0 : ctp_value.hashCode());
		result = prime * result + ((period == null) ? 0 : period.hashCode());
		result = prime * result + rank;
		result = prime * result + ((system == null) ? 0 : system.hashCode());
		result = prime * result + ((use == null) ? 0 : use.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContactPoint other = (ContactPoint) obj;
		if (ctp_value == null) {
			if (other.ctp_value != null)
				return false;
		} else if (!ctp_value.equals(other.ctp_value))
			return false;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		if (rank != other.rank)
			return false;
		if (system != other.system)
			return false;
		if (use != other.use)
			return false;
		return true;
	}
	
	
}