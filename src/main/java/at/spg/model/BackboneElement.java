package at.spg.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass //dass dann eine tabelle erstellt wird mit allen
public class BackboneElement extends Element{
	
	
	public BackboneElement() {
		super();
	}

	public BackboneElement(Set<Extension> modifierExtension) {
		super();
		this.modifierExtension = modifierExtension;
	}

	@OneToMany(cascade = CascadeType.ALL, targetEntity = Extension.class)
	@JoinColumn(name= "modifierExtension_backbone", referencedColumnName = "id")
    private Set<Extension> modifierExtension = new HashSet<>();

	public Set<Extension> getModifierExtension() {
		return modifierExtension;
	}

	public void setModifierExtension(Set<Extension> modifierExtension) {
		this.modifierExtension = modifierExtension;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((modifierExtension == null) ? 0 : modifierExtension.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		BackboneElement other = (BackboneElement) obj;
		if (modifierExtension == null) {
			if (other.modifierExtension != null)
				return false;
		} else if (!modifierExtension.equals(other.modifierExtension))
			return false;
		return true;
	}
	
	
}