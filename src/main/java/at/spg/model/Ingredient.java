package at.spg.model;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Entity
public class Ingredient extends BackboneElement{
    public Ingredient() { super(); }

    public Ingredient(CodeableConcept itemCodeableConcept, Boolean isactive, Ratio strength) {
        this.itemReference = itemReference;
        this.isactive = isactive;
        this.strength = strength;
    }
    public Ingredient(Reference itemReference, Boolean isactive, Ratio strength) {
        this.itemReference = itemReference;
        this.isactive = isactive;
        this.strength = strength;
    }


    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="ingredient_itemCodeableConcept", referencedColumnName = "id")
    private  CodeableConcept  itemCodeableConcept;

    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name= "ingredient_itemReference_fk", referencedColumnName = "id")
    private Reference itemReference;

    @Column(name="ingredient_isactive")
    private Boolean isactive;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="ingredient_strength", referencedColumnName = "id")
    private Ratio strength;

    public CodeableConcept getItemCodeableConcept() {
        return itemCodeableConcept;
    }

    public void setItemCodeableConcept(CodeableConcept itemCodeableConcept) {
        if(getItemReference() == null) {
            this.itemCodeableConcept = itemCodeableConcept;
        } else {
            throw new IllegalArgumentException("itemCodeableConcept and itemReference cannot be set at the same time");
        }
    }

    public Reference getItemReference() {
        return itemReference;
    }

    public void setItemReference(Reference itemReference) {
        if(getItemCodeableConcept() == null) {
            this.itemReference = itemReference;
        } else {
            throw new IllegalArgumentException("itemReference and itemCodeableConcept cannot be set at the same time");
        }
    }

    public Boolean getIsactive() {
        return isactive;
    }

    public void setIsactive(Boolean isactive) {
        this.isactive = isactive;
    }

    public Ratio getStrength() {
        return strength;
    }

    public void setStrength(Ratio strength) {
        this.strength = strength;
    }
}
