package at.spg.model;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
public class Ratio extends Element{
    public Ratio() { super();   }

    public Ratio(Quantity numerator, Quantity denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
    }

    @OneToOne(cascade= CascadeType.ALL)
    @JoinColumn(name="ratio_numerator_fk")
    private Quantity numerator;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="ratio_denominator_fk", referencedColumnName = "id")
    private Quantity denominator;

    public Quantity getNumerator() {
        return numerator;
    }

    public void setNumerator(Quantity numerator) {
        this.numerator = numerator;
    }

    public Quantity getDenominator() {
        return denominator;
    }

    public void setDenominator(Quantity denominator) {
        this.denominator = denominator;
    }
}
