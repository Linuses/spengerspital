package at.spg.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import java.time.LocalDateTime;
import java.util.Objects;


@Embeddable
public class DeviceTiming{
    public DeviceTiming() {super();}

    public DeviceTiming(LocalDateTime oftenDateTime, Period period) {
        this.oftenDateTime = oftenDateTime;
        this.period = period;
    }

    @Column(name = "DeviceTiming_oftenDateTime")
    private LocalDateTime oftenDateTime;

    @Embedded
    private Period period;

    public LocalDateTime getOftenDateTime() {
        return oftenDateTime;
    }

    public void setOftenDateTime(LocalDateTime oftenDateTime) {
        this.oftenDateTime = oftenDateTime;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceTiming that = (DeviceTiming) o;
        return Objects.equals(oftenDateTime, that.oftenDateTime) &&
                Objects.equals(period, that.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(oftenDateTime, period);
    }


}
