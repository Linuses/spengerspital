package at.spg.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

//@NoArgsConstructor
//@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public @Data
class
DeviceUseStatement extends DomainResource {
    public DeviceUseStatement(Set<Identifier> identifier, Set<Reference> basedOn, StatusCode status, DeviceTiming timing, LocalDateTime recordedDate) {
        this.identifier = identifier;
        this.basedOn = basedOn;
        this.status = status;
        this.timing = timing;
        this.recordedDate = recordedDate;
    }

    public DeviceUseStatement() { super();
    }

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
    @JoinColumn(name = "identifier_deviceUseStatement_fk", referencedColumnName = "id")
    private Set<Identifier> identifier = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Reference.class)
    @JoinColumn(name="ref_deviceUseStatement_fk", referencedColumnName = "id")
    private Set<Reference> basedOn = new HashSet<>();



    public enum StatusCode {
        acting("acting"),
        compelled("compelled"),
        enteredinending("entered-in-ending");

        private String value;
        private StatusCode(String value)
        {this.value = value;}

        public String toString()
        {return this.value; }
    }

    @Enumerated(EnumType.STRING)
    @Column(name="deviceUseStatement_stats")
    private DeviceUseStatement.StatusCode status = DeviceUseStatement.StatusCode.acting;



    @Embedded
    private DeviceTiming timing;

    @Column(name="deviceUseStatement_recordedDate")
    private LocalDateTime recordedDate;
}
