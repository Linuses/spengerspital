package at.spg.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity
public class Address extends Element{
	
	public Address() { }
	
	public Address(UseCode use, TypeCode type, String text, Set<String> line, String city, 
	String district,String state, String postalCode, String country, Period period) {
		super();
		this.use = use;
		this.type = type;
		this.text = text;
		this.line = line;
		this.city = city;
		this.district = district;
		this.state = state;
		this.postalCode = postalCode;
		this.country = country;
		this.period = period;
	}

	public enum UseCode {
		home , work , temp , old , billing //- purpose of this address
		//AddressUse (Required)
	}
	
	public enum TypeCode {
		postal , physical , both
		//AddressUse (Required)
	}

    @Enumerated(EnumType.STRING)
	@Column(name="add_use")
	private UseCode use = UseCode.temp;

    @Enumerated(EnumType.STRING)
	@Column(name="add_type")
	private TypeCode type = TypeCode.both;
	
	@Column(name="add_text")
	private String text;
	
	@ElementCollection
    @CollectionTable(name = "add_line", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "add_line")
    private Set<String> line = new HashSet<>();
		
	@Column(name="add_city")
	private String city;
	
	@Column(name="add_district")
	private String district;
	
	@Column(name="add_state")
	private String state;
	
	@Column(name="add_postalCode")
	private String postalCode;

	@Column(name="add_country")
	private String country;
	
	@Column(name ="add_period")
	private Period period;

	public UseCode getUse() {
		return use;
	}

	public void setUse(UseCode use) {
		this.use = use;
	}

	public TypeCode getType() {
		return type;
	}

	public void setType(TypeCode type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Set<String> getLine() {
		return line;
	}

	public void setLine(Set<String> line) {
		this.line = line;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}




	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((district == null) ? 0 : district.hashCode());
		result = prime * result + ((line == null) ? 0 : line.hashCode());
		result = prime * result + ((period == null) ? 0 : period.hashCode());
		result = prime * result + ((postalCode == null) ? 0 : postalCode.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + ((use == null) ? 0 : use.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Address other = (Address) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (district == null) {
			if (other.district != null)
				return false;
		} else if (!district.equals(other.district))
			return false;
		if (line == null) {
			if (other.line != null)
				return false;
		} else if (!line.equals(other.line))
			return false;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		if (postalCode == null) {
			if (other.postalCode != null)
				return false;
		} else if (!postalCode.equals(other.postalCode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		if (type != other.type)
			return false;
		if (use != other.use)
			return false;
		return true;
	}
	
	
}