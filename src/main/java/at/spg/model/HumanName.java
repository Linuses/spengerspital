package at.spg.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;


@Entity
public class HumanName extends Element{

	public HumanName() { super();
	}
	

	public HumanName(String text, UseCode use, Set<String> given, Set<String> prefix, Set<String> suffix,
			Period period, String family) {
		super();
		this.text = text;
		this.use = use;
		this.given = given;
		this.prefix = prefix;
		this.suffix = suffix;
		this.period = period;
		this.family = family;
	}

	public enum UseCode{
		usual , official ,temp , nickname , anonymous , old , maiden
		//NameUse (Required)
    }

    @Column(name="hun_text") 
    private String text;
    
    @Enumerated(EnumType.STRING)
    @Column(name="hun_use")
    private UseCode use=UseCode.anonymous;
    
    @Column(name="hun_family")
	public String family;
    
    @ElementCollection //TODO: set<enum> practitionerrole
    @CollectionTable(name= "human_name_given", joinColumns = @JoinColumn(name="id"))
    @Column(name = "hn_given")
    private Set<String> given = new HashSet<>();
    
    @ElementCollection
    @CollectionTable(name= "human_name_prefix", joinColumns = @JoinColumn(name="id"))
    @Column(name = "hn_prefix")
    private Set<String> prefix = new HashSet<>();
    
    @ElementCollection
    @CollectionTable(name= "human_name_suffix", joinColumns = @JoinColumn(name="id"))
    @Column(name = "hn_suffix")
    private Set<String> suffix = new HashSet<>();
    
    @Column(name ="hun_period")
    private Period period;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UseCode getUse() {
        return use;
    }

    public void setUse(UseCode use) {
        this.use = use;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public Set<String> getGiven() {
        return given;
    }

    public void setGiven(Set<String> given) {
        this.given = given;
    }

    public Set<String> getPrefix() {
        return prefix;
    }

    public void setPrefix(Set<String> prefix) {
        this.prefix = prefix;
    }

    public Set<String> getSuffix() {
        return suffix;
    }

    public void setSuffix(Set<String> suffix) {
        this.suffix = suffix;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        HumanName humanName = (HumanName) o;
        return Objects.equals(text, humanName.text) &&
                use == humanName.use &&
                Objects.equals(family, humanName.family) &&
                Objects.equals(given, humanName.given) &&
                Objects.equals(prefix, humanName.prefix) &&
                Objects.equals(suffix, humanName.suffix) &&
                Objects.equals(period, humanName.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), text, use, family, given, prefix, suffix, period);
    }
}
