package at.spg.model;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity //eine tabelle in SQL machen
public class Patient extends DomainResource{

	//Project Lombok
	
    public Patient(){super();}
    
    
    //er hat nur alles bis auf identifier und noch phonenumbers
    
//TODO  update generalPractitioner everywhere it is getting used
    public Patient(boolean active, GenderCode gender, LocalDate birthDate,
				   Set<Identifier> identifier,
			Set<HumanName> name, Set<ContactPoint> telecom, LocalDateTime deceasedDateTime,
				   Set<Address> address,
			Set<Attachment> photo, Set<Practitioner> generalPractitioner) {
		super();
		this.active = active;
		this.gender = gender;
		this.birthDate = birthDate;
		this.identifier = identifier;
		this.name = name;
		this.telecom = telecom;
		this.deceasedDateTime = deceasedDateTime;
		this.address = address;
		this.photo = photo;
		this.generalPractitioner = generalPractitioner;
	}
	

    
//    public Patient(boolean active, GenderCode gender, LocalDate birthDate, Set<Identifier> identifier,
//    		Set<ContactPoint> telecom		, Set<HumanName> name) {
//		super();
//		this.active = active;
//		this.gender = gender;
//		this.birthDate = birthDate;
//		this.identifier = identifier;
//		this.telecom = telecom;
//		this.name = name;
//	}

	public enum GenderCode{
        male,female,other,unknown
    }

    @Column(name="p_active") 
    private boolean active;
    
    @Enumerated(EnumType.STRING)
    @Column(name="p_gender")
    private GenderCode gender;

    @Column(name="p_birthdate")
    private LocalDate birthDate;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
	@JoinColumn(name= "i_p_fk", referencedColumnName = "id")
    private Set<Identifier> identifier= new HashSet<>() ;;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = ContactPoint.class)
	@JoinColumn(name= "ctp_p_fk", referencedColumnName = "id")
    private Set<ContactPoint> telecom = new HashSet<>() ;

    //deceased ist ein Feld mit Fragezeichen (FHIR definiert das JSON)
    //in dem JSON darf nur boolean oder DateTime drinnen sein
   //man müsste Controller so bauen, dass er nur eines ausspuckt
    //(in der DB könnte man beides speichern)
    //wir sagen wir verwenden nur das deceasedDateTime
    //zum umsetzen der Beiden bräuchte man 2 Controller und 2 Models
    
    @Column(name="p_deceasedDateTime") //in so einem fall einfach eines von den beiden nehmen
    private LocalDateTime deceasedDateTime;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Address.class)
	@JoinColumn(name= "addre_p_fk", referencedColumnName = "id")
    private Set<Address> address= new HashSet<>() ;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Attachment.class)
	@JoinColumn(name= "att_p_fk", referencedColumnName = "id")
    private Set<Attachment> photo= new HashSet<>() ;
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = Practitioner.class)
	@JoinColumn(name= "prac_generalPractitioner_p_fk", referencedColumnName = "id")
    private Set<Practitioner> generalPractitioner= new HashSet<>() ;
    
    
    @OneToMany(cascade = CascadeType.ALL, targetEntity = HumanName.class)
    @JoinColumn(name = "name_p_fk", referencedColumnName="id")
    private Set<HumanName> name = new HashSet<>();
    
    public boolean isActive() {
    	return active;
    }
    
    public void setActive(boolean active) {
        this.active = active;
    }

    public GenderCode getGender() {
        return gender;
    }

    public void setGender(GenderCode gender) {
        this.gender = gender;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

	public Set<Identifier> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Set<Identifier> identifier) {
		this.identifier = identifier;
	}

	public Set<HumanName> getName() {
		return name;
	}


	public void setName(Set<HumanName> name) {
		this.name = name;
	}


	public Set<ContactPoint> getTelecom() {
		return telecom;
	}

	public void setTelecom(Set<ContactPoint> telecom) {
		this.telecom = telecom;
	}

	public LocalDateTime getDeceasedDateTime() {
		return deceasedDateTime;
	}

	public void setDeceasedDateTime(LocalDateTime deceasedDateTime) {
		this.deceasedDateTime = deceasedDateTime;
	}

	public Set<Address> getAddress() {
		return address;
	}

	public void setAddress(Set<Address> address) {
		this.address = address;
	}

	public Set<Attachment> getPhoto() {
		return photo;
	}

	public void setPhoto(Set<Attachment> photo) {
		this.photo = photo;
	}

	public Set<Practitioner> getGeneralPractitioner() {
		return generalPractitioner;
	}

	public void setGeneralPractitioner(Set<Practitioner> generalPractitioner) {
		this.generalPractitioner = generalPractitioner;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((deceasedDateTime == null) ? 0 : deceasedDateTime.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((generalPractitioner == null) ? 0 : generalPractitioner.hashCode());
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		result = prime * result + ((telecom == null) ? 0 : telecom.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patient other = (Patient) obj;
		if (active != other.active)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (deceasedDateTime == null) {
			if (other.deceasedDateTime != null)
				return false;
		} else if (!deceasedDateTime.equals(other.deceasedDateTime))
			return false;
		if (gender != other.gender)
			return false;
		if (generalPractitioner == null) {
			if (other.generalPractitioner != null)
				return false;
		} else if (!generalPractitioner.equals(other.generalPractitioner))
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		if (telecom == null) {
			if (other.telecom != null)
				return false;
		} else if (!telecom.equals(other.telecom))
			return false;
		return true;
	}


	
	
}
