package at.spg.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import at.spg.model.Patient.GenderCode;

@Entity
public class Practitioner extends DomainResource{
	
	public Practitioner() {
		super();
	}
	
	public Practitioner(Set<Identifier> identifier, boolean active, Set<HumanName> name,
						Set<ContactPoint> telecom,
			Set<Address> address, GenderCode gender, LocalDate birthDate, Set<Attachment> photo,
			Set<Qualification> qualification, Set<CodeableConcept> communication) {
		super();
		this.identifier = identifier;
		this.active = active;
		this.name = name;
		this.telecom = telecom;
		this.address = address;
		this.gender = gender;
		this.birthDate = birthDate;
		this.photo = photo;
		this.qualification = qualification;
		this.communication = communication;
	}
	
	public Practitioner(Set<Identifier> identifier, boolean active, Set<HumanName> name, Set<ContactPoint> telecom,
			Set<Address> address, GenderCode gender, LocalDate birthDate) {
		super();
		this.identifier = identifier;
		this.active = active;
		this.name = name;
		this.telecom = telecom;
		this.address = address;
		this.gender = gender;
		this.birthDate = birthDate;
	}


	public enum GenderCode{
        male,female,other,unknown
    }
	
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
	@JoinColumn(name= "i_prac_fk", referencedColumnName = "id")
	private Set<Identifier> identifier = new HashSet<>();
	
	@Column(name="prac_active") 
    private boolean active;
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = HumanName.class)
    @JoinColumn(name = "name_prac_fk", referencedColumnName="id")
    private Set<HumanName> name = new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = ContactPoint.class)
	@JoinColumn(name= "ctp_prac_fk", referencedColumnName = "id")
    private Set<ContactPoint> telecom = new HashSet<>() ;
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Address.class)
	@JoinColumn(name= "addr_prac_fk", referencedColumnName = "id")
    private Set<Address> address = new HashSet<>();
	
	@Enumerated(EnumType.STRING)
    @Column(name="prac_gender")
    private GenderCode gender=GenderCode.unknown;
	
	@Column(name="p_birthdate")
    private LocalDate birthDate;
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Attachment.class)
	@JoinColumn(name= "attaphoto_prac_fk", referencedColumnName = "id")
    private Set<Attachment> photo = new HashSet<>();
	
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = Qualification.class)
	@JoinColumn(name= "qual_prac_fk", referencedColumnName = "id")
    private Set<Qualification> qualification = new HashSet<>();
	
	@OneToMany(cascade = CascadeType.ALL, targetEntity = CodeableConcept.class)
	@JoinColumn(name= "cccomuni_prac_fk", referencedColumnName = "id")
    private Set<CodeableConcept> communication = new HashSet<>();


	public Set<Identifier> getIdentifier() {
		return identifier;
	}


	public void setIdentifier(Set<Identifier> identifier) {
		this.identifier = identifier;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public Set<HumanName> getName() {
		return name;
	}


	public void setName(Set<HumanName> name) {
		this.name = name;
	}


	public Set<ContactPoint> getTelecom() {
		return telecom;
	}


	public void setTelecom(Set<ContactPoint> telecom) {
		this.telecom = telecom;
	}


	public Set<Address> getAddress() {
		return address;
	}


	public void setAddress(Set<Address> address) {
		this.address = address;
	}


	public GenderCode getGender() {
		return gender;
	}


	public void setGender(GenderCode gender) {
		this.gender = gender;
	}


	public LocalDate getBirthDate() {
		return birthDate;
	}


	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}


	public Set<Attachment> getPhoto() {
		return photo;
	}


	public void setPhoto(Set<Attachment> photo) {
		this.photo = photo;
	}

	public Set<Qualification> getQualification() {
		return qualification;
	}

	public void setQualification(Set<Qualification> qualification) {
		this.qualification = qualification;
	}

	public Set<CodeableConcept> getCommunication() {
		return communication;
	}


	public void setCommunication(Set<CodeableConcept> communication) {
		this.communication = communication;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((communication == null) ? 0 : communication.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((photo == null) ? 0 : photo.hashCode());
		result = prime * result + ((qualification == null) ? 0 : qualification.hashCode());
		result = prime * result + ((telecom == null) ? 0 : telecom.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Practitioner other = (Practitioner) obj;
		if (active != other.active)
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (birthDate == null) {
			if (other.birthDate != null)
				return false;
		} else if (!birthDate.equals(other.birthDate))
			return false;
		if (communication == null) {
			if (other.communication != null)
				return false;
		} else if (!communication.equals(other.communication))
			return false;
		if (gender != other.gender)
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (photo == null) {
			if (other.photo != null)
				return false;
		} else if (!photo.equals(other.photo))
			return false;
		if (qualification == null) {
			if (other.qualification != null)
				return false;
		} else if (!qualification.equals(other.qualification))
			return false;
		if (telecom == null) {
			if (other.telecom != null)
				return false;
		} else if (!telecom.equals(other.telecom))
			return false;
		return true;
	}
	
	
}
