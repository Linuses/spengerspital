package at.spg.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import at.spg.model.Patient.GenderCode;

@Entity
public class Encounter extends DomainResource { //DomainResource machen
    public Encounter() {
        super();
    }

    public Encounter(Set<Identifier> identifier, StatusCode status, Set<Statushistory> statusHistory,
                     Set<CodeableConcept> type, Coding classfromenc, Reference subject,
                     Set<Reference> episodeOfCare, Set<Participant> participant,
                     Set<Reference> appointment, Period period, Set<Diagnosis> diagnosis,
                     Reference partOf) {
        this.identifier = identifier;
        this.status = status;
        this.statusHistory = statusHistory;
        this.type = type;
        this.classfromenc = classfromenc;
        this.subject = subject;
        this.episodeOfCare = episodeOfCare;
        this.participant = participant;
        this.appointment = appointment;
        this.period = period;
        this.diagnosis = diagnosis;
        this.partOf = partOf;
    }

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
    @JoinColumn(name = "i_enc_fk", referencedColumnName = "id")
    private Set<Identifier> identifier;

    public enum StatusCode {
        planned, arrived, triaged, inprogress, onleave, finished, cancelled //in-progress
    }

    //@OneToOne
    @Enumerated(EnumType.STRING)
    @Column(name = "enc_status")
    private StatusCode status = StatusCode.onleave;

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Statushistory.class)
    @JoinColumn(name = "statusHistory_enc_fk", referencedColumnName = "id",nullable = false)
    private Set<Statushistory> statusHistory = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, targetEntity = CodeableConcept.class)
    @JoinColumn(name = "type_enc_fk", referencedColumnName = "id")
    private Set<CodeableConcept> type = new HashSet<>();

    @OneToOne
    @JoinColumn(name = "enc_coding_fk")
    private Coding classfromenc;

    @OneToOne//(cascade=CascadeType.ALL) //https://stackoverflow.com/questions/2302802/how-to-fix-the-hibernate-object-references-an-unsaved-transient-instance-save
    @JoinColumn(name = "enc_subject_fk")
    private Reference subject;

    @OneToMany(cascade = CascadeType.ALL,targetEntity=Reference.class)
    @JoinColumn(name = "ref_epiOfCare_enc_fk", referencedColumnName = "id")
    private Set<Reference> episodeOfCare = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, targetEntity = Participant.class)
    @JoinColumn(name = "participant_enc_fk", referencedColumnName = "id")
    private Set<Participant> participant = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,targetEntity=Reference.class)
    @JoinColumn(name = "ref_appointment_fk", referencedColumnName = "id")
    private Set<Reference> appointment = new HashSet<>();

    //wenn was man einbettet keine Daseinsberechtigung hat
    //existiert nur wenn der encounter existiert
    //man spart sich eine Tabelle
    @Embedded
    private Period period;

    @OneToMany(cascade = CascadeType.ALL,targetEntity = Diagnosis.class)
    @JoinColumn(name = "diagnosis_enc_fk", referencedColumnName = "id")
    private Set<Diagnosis> diagnosis = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "enc_partOf_fk", referencedColumnName = "id")
    private Reference partOf;

    public Set<Identifier> getIdentifier() {
        return identifier;
    }

    public void setIdentifier(Set<Identifier> identifier) {
        this.identifier = identifier;
    }

    public StatusCode getStatus() {
        return status;
    }

    public void setStatus(StatusCode status) {
        this.status = status;
    }

    public Set<Statushistory> getStatusHistory() {
        return statusHistory;
    }

    public void setStatusHistory(Set<Statushistory> statusHistory) {
        this.statusHistory = statusHistory;
    }

    public Set<CodeableConcept> getType() {
        return type;
    }

    public void setType(Set<CodeableConcept> type) {
        this.type = type;
    }

    public Coding getClassfromenc() {
        return classfromenc;
    }

    public void setClassfromenc(Coding classfromenc) {
        this.classfromenc = classfromenc;
    }

    public Reference getSubject() {
        return subject;
    }

    public void setSubject(Reference subject) {
        this.subject = subject;
    }

    public Set<Reference> getEpisodeOfCare() {
        return episodeOfCare;
    }

    public void setEpisodeOfCare(Set<Reference> episodeOfCare) {
        this.episodeOfCare = episodeOfCare;
    }

    public Set<Participant> getParticipant() {
        return participant;
    }

    public void setParticipant(Set<Participant> participant) {
        this.participant = participant;
    }

    public Set<Reference> getAppointment() {
        return appointment;
    }

    public void setAppointment(Set<Reference> appointment) {
        this.appointment = appointment;
    }

    public Period getPeriod() {
        return period;
    }

    public void setPeriod(Period period) {
        this.period = period;
    }

    public Set<Diagnosis> getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(Set<Diagnosis> diagnosis) {
        this.diagnosis = diagnosis;
    }

    public Reference getPartOf() {
        return partOf;
    }

    public void setPartOf(Reference partOf) {
        this.partOf = partOf;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Encounter encounter = (Encounter) o;
        return Objects.equals(identifier, encounter.identifier) &&
                status == encounter.status &&
                Objects.equals(statusHistory, encounter.statusHistory) &&
                Objects.equals(type, encounter.type) &&
                Objects.equals(classfromenc, encounter.classfromenc) &&
                Objects.equals(subject, encounter.subject) &&
                Objects.equals(episodeOfCare, encounter.episodeOfCare) &&
                Objects.equals(participant, encounter.participant) &&
                Objects.equals(appointment, encounter.appointment) &&
                Objects.equals(period, encounter.period) &&
                Objects.equals(diagnosis, encounter.diagnosis) &&
                Objects.equals(partOf, encounter.partOf);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), identifier, status, statusHistory, type, classfromenc, subject, episodeOfCare, participant, appointment, period, diagnosis, partOf);
    }
}
