package at.spg.model;

import java.net.URL;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Attachment extends Element{
	
	public Attachment() {super();}
	
	public Attachment(ContentTypeCode contentType, LanguageCode language, String data, 
	String url, Long size, String hash,
			String title, LocalDateTime creation) {
		super();
//		this.contentType = contentType;
		this.language = language;
		this.data = data;
		this.url = url;
		this.size = size;
		this.hash = hash;
		this.title = title;
		this.creation = creation;
	}

	public enum ContentTypeCode {
		urn
		//Mime type of the content, with charset etc.
		//MimeType (Required)
	}

	public enum LanguageCode {
		ar, bn, cs, da, de
		//AddressUse (Required)
		//https://www.hl7.org/fhir/valueset-languages.html
	}

    //@Enumerated(EnumType.STRING)
	//@Column(name="atta_contentType")
	//public ContentTypeCode contentType = ContentTypeCode.urn;
    
    @Enumerated(EnumType.STRING)
	@Column(name="atta_language")
	private LanguageCode language = LanguageCode.ar;
    
	@Column(name="atta_data")
	private String data;
	
	@Column(name="atta_url")
	private String url;
	
	@Column(name="atta_size")
	private Long size;

	@Column(name="atta_hash")
	private String hash;
	
	@Column(name="atta_title")
	private String title;
	
	@Column(name="atta_creation")
	private LocalDateTime creation;

	public LanguageCode getLanguage() {
		return language;
	}

	public void setLanguage(LanguageCode language) {
		this.language = language;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}


	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public LocalDateTime getCreation() {
		return creation;
	}

	public void setCreation(LocalDateTime creation) {
		this.creation = creation;
	}



	
	
	
}