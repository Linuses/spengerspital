package at.spg.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
@Entity
public class Qualification extends BackboneElement{

	public Qualification(Set<Extension> modifierExtension, Set<Identifier> identifier, 
	CodeableConcept code,
			Period period) {
		super(modifierExtension);
		this.identifier = identifier;
		this.code = code;
		this.period = period;
	}

	public Qualification() { super();
	}

	@OneToMany(cascade = CascadeType.ALL, targetEntity = Identifier.class)
	@JoinColumn(name= "i_qual_fk", referencedColumnName = "id")
    private Set<Identifier> identifier = new HashSet<>();
	
	@OneToOne(cascade=CascadeType.ALL) //alle Referenzen beachten
	@JoinColumn(name= "qualific_code_fk", referencedColumnName = "id")
	private CodeableConcept code;
	
	@Embedded
	private Period period;

	public Set<Identifier> getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Set<Identifier> identifier) {
		this.identifier = identifier;
	}

	public CodeableConcept getCode() {
		return code;
	}

	public void setCode(CodeableConcept code) {
		this.code = code;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
		result = prime * result + ((period == null) ? 0 : period.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Qualification other = (Qualification) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (identifier == null) {
			if (other.identifier != null)
				return false;
		} else if (!identifier.equals(other.identifier))
			return false;
		if (period == null) {
			if (other.period != null)
				return false;
		} else if (!period.equals(other.period))
			return false;
		return true;
	}
	
	
}