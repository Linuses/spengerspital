package at.spg.controller;

import at.spg.model.Flag;
import at.spg.model.Flag;
import at.spg.repositories.FlagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path="/api/flag")
public class FlagController {

    @Autowired
    private FlagRepository flagRepository;

    @GetMapping
    public @ResponseBody
    Iterable<Flag> getAllFlags(){
        return flagRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity <Flag> getFlag(@PathVariable String id){
        return flagRepository
                .findById(id)
                .map(flag -> ResponseEntity.ok().body(flag))
                .orElse(ResponseEntity.notFound().build());
    }


    @PostMapping()
    @Async //eigenere Prozess, nicht notwendig
    public ResponseEntity<Flag> createFlag(@Valid @RequestBody Flag medication) {
        var saved = flagRepository.save(medication);
        return ResponseEntity.created(URI.create("/api/Flag/" + saved.getId())).body(saved);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Flag> deleteFlag(@PathVariable(value = "id") String flagId) {
        return flagRepository
                .findById(flagId)
                .map(
                        flag -> {
                            flagRepository.delete(flag);
                            return ResponseEntity.ok().<Flag>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Flag> updateFlag(@PathVariable(value = "id") String id, @Valid @RequestBody Flag flagDetails) {
        return flagRepository.
                findById(id)
                .map(flag ->{

                    flag.setIdentifier(flagDetails.getIdentifier());
                    flag.setStatus(flagDetails.getStatus());
                    flag.setOther(flagDetails.getOther());
                    flag.setPeriod(flagDetails.getPeriod());

                            Flag updatedFlag = flagRepository.save(flag);
                            return ResponseEntity.ok(updatedFlag);
                        }
                ).orElseGet(() -> createFlag(flagDetails)); //
    }
}
