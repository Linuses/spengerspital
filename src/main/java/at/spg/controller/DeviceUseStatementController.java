package at.spg.controller;

import at.spg.model.DeviceUseStatement;
import at.spg.model.DeviceUseStatement;
import at.spg.repositories.DeviceUseStatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path="/api/deviceUseStatement")
public class DeviceUseStatementController {

    @Autowired
    private DeviceUseStatementRepository deviceUseStatementRepository;

    @GetMapping
    public @ResponseBody
    Iterable<DeviceUseStatement> getAllDeviceUseStatements(){
        return deviceUseStatementRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity <DeviceUseStatement> getDeviceUseStatement(@PathVariable String id){
        return deviceUseStatementRepository
                .findById(id)
                .map(deviceUseStatement -> ResponseEntity.ok().body(deviceUseStatement))
                .orElse(ResponseEntity.notFound().build());
    }


    @PostMapping()
    public ResponseEntity<DeviceUseStatement> createDeviceUseStatement(@Valid @RequestBody DeviceUseStatement deviceUseStatement) {
        var saved = deviceUseStatementRepository.save(deviceUseStatement);
        return ResponseEntity.created(URI.create("api/deviceUseStatement/" + saved.getId())).body(saved);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<DeviceUseStatement> deleteDeviceUseStatement(@PathVariable(value = "id") String deviceUseStatementId) {
        return deviceUseStatementRepository
                .findById(deviceUseStatementId)
                .map(
                        deviceUseStatement -> {
                            deviceUseStatementRepository.delete(deviceUseStatement);
                            return ResponseEntity.ok().<DeviceUseStatement>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<DeviceUseStatement> updateDeviceUseStatement(@PathVariable(value = "id") String id, @Valid @RequestBody DeviceUseStatement deviceUseStatementDetails) {
        return deviceUseStatementRepository.
                findById(id)
                .map(deviceUseStatement ->{

                    deviceUseStatement.setIdentifier(deviceUseStatementDetails.getIdentifier());
                    deviceUseStatement.setBasedOn(deviceUseStatementDetails.getBasedOn());
                    deviceUseStatement.setStatus(deviceUseStatementDetails.getStatus());
                    deviceUseStatement.setTiming(deviceUseStatementDetails.getTiming());
                    deviceUseStatement.setRecordedDate(deviceUseStatementDetails.getRecordedDate());
                            DeviceUseStatement updatedDeviceUseStatement = deviceUseStatementRepository.save(deviceUseStatement);
                            return ResponseEntity.ok(updatedDeviceUseStatement);
                        }
                ).orElseGet(() -> createDeviceUseStatement(deviceUseStatementDetails)); //
    }
}
