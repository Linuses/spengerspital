package at.spg.controller;

import at.spg.model.Patient;
import at.spg.model.Practitioner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import at.spg.model.Patient;
import at.spg.repositories.PatientRepository;

import javax.validation.Valid;
import java.net.URI;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path="/api/patient")
@CrossOrigin(origins = "http://localhost:4200")
//https://spring.io/guides/gs/rest-service-cors/
public class PatientController {
    @Autowired
    private PatientRepository patientRepository;

    //Get all Patients
    @GetMapping
    public @ResponseBody Iterable<Patient> getAllPatients(){
        // This returns a JSON or a XML
        return patientRepository.findAll();
    }

    //Get Patient by ID
    @GetMapping("/{id}")
    public ResponseEntity <Patient> getPatient(@PathVariable String id){
        return patientRepository
                .findById(id)
                .map(patient -> ResponseEntity.ok().body(patient))
                .orElse(ResponseEntity.notFound().build());
    }


    @PostMapping()
    public ResponseEntity<Patient> createPatient(@Valid @RequestBody Patient patient) {
        var saved = patientRepository.save(patient);
        return ResponseEntity.created(URI.create("api/patient/" + saved.getId())).body(saved);
    }

    // Delete a Patient
    @DeleteMapping("/{id}")
    public ResponseEntity<Patient> deletePatient(@PathVariable(value = "id") String patientId) {
        return patientRepository
                .findById(patientId)
                .map(
                        patient -> {
                            patientRepository.delete(patient);
                            return ResponseEntity.ok().<Patient>build();
                        })
                .orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Patient> updatePatient(@PathVariable(value = "id") String id, @Valid @RequestBody Patient patientDetails) {
        return patientRepository.
                findById(id)
                .map(patient ->{

                patient.setActive(patientDetails.isActive());
                patient.setGender(patientDetails.getGender());
                patient.setBirthDate(patientDetails.getBirthDate());
                patient.setIdentifier(patientDetails.getIdentifier());
                patient.setTelecom(patientDetails.getTelecom());
                patient.setName(patientDetails.getName());
                patient.setTelecom(patientDetails.getTelecom());
                patient.setDeceasedDateTime(patientDetails.getDeceasedDateTime());
                patient.setAddress(patientDetails.getAddress());
                patient.setPhoto(patientDetails.getPhoto());
                patient.setGeneralPractitioner(patientDetails.getGeneralPractitioner());

                        Patient updatedPatient = patientRepository.save(patient);
                        return ResponseEntity.ok(updatedPatient);
                        }
                ).orElseGet(() -> createPatient(patientDetails)); //
    }
}
