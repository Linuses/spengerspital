package at.spg.controller;

import at.spg.model.PractitionerRole;
import at.spg.repositories.PracRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path="/api/practitionerRole") // This means URL's start with /demo (after Application path)
public class PracRoleController {

    @Autowired
    private PracRoleRepository practitionerRoleRepository;

    //Get all PractitionerRoles
    @GetMapping
    public @ResponseBody Iterable<PractitionerRole> getAllPractitionerRoles(){
        // This returns a JSON or a XML
        return practitionerRoleRepository.findAll();
    }

    //Get PractitionerRole by ID
    @GetMapping("/{id}")
    public ResponseEntity <PractitionerRole> getPractitionerRole(@PathVariable String id){
        return practitionerRoleRepository
                .findById(id)
                .map(practitionerRole -> ResponseEntity.ok().body(practitionerRole))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping()
    public ResponseEntity<PractitionerRole> createPractitionerRole(@Valid @RequestBody PractitionerRole practitionerRole) {
        var saved = practitionerRoleRepository.save(practitionerRole);
        return ResponseEntity.created(URI.create("api/practitionerRole/" + saved.getId())).body(saved);
    }

    //Delete an PractitionerRole
    @DeleteMapping("/{id}")
    public ResponseEntity<PractitionerRole> deletePractitionerRole(@PathVariable(value = "id") String practitionerRoleId) {
        return practitionerRoleRepository
                .findById(practitionerRoleId)
                .map(
                        practitionerRole -> {
                            practitionerRoleRepository.delete(practitionerRole);
                            return ResponseEntity.ok().<PractitionerRole>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<PractitionerRole> updatePractitionerRole(@PathVariable(value = "id") String id, @Valid @RequestBody PractitionerRole practitionerRoleDetails) {
        return practitionerRoleRepository.
                findById(id)
                .map(practitionerRole ->{
                    practitionerRole.setIdentifier(practitionerRoleDetails.getIdentifier());
                    practitionerRole.setActive(practitionerRoleDetails.getActive());
                    practitionerRole.setPeriod(practitionerRoleDetails.getPeriod());
                    practitionerRole.setPractitioner(practitionerRoleDetails.getPractitioner());
                    practitionerRole.setAvailableTime(practitionerRoleDetails.getAvailableTime());
                    practitionerRole.setSpeciality(practitionerRoleDetails.getSpeciality());

                            PractitionerRole updatedPractitionerRole = practitionerRoleRepository.save(practitionerRole);
                            return ResponseEntity.ok(updatedPractitionerRole);
                        }
                ).orElseGet(() -> createPractitionerRole(practitionerRoleDetails)); //
    }
}