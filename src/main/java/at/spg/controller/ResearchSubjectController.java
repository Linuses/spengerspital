package at.spg.controller;

import at.spg.model.ResearchSubject;
import at.spg.repositories.ResearchSubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RequestMapping(path="/api/researchSubject")
@RestController //REST: beschreibt Mechanismus dass daten über http ausgetauscht werden
@CrossOrigin(origins = "http://localhost:4200")
//stateless: requests unabhängig von vorherigen requests
public class ResearchSubjectController {
 	@Autowired //singleton: dass es nur eine Instanz davon gibt  und  =new Practio...
    private ResearchSubjectRepository researchSubjectRepository;

    @GetMapping
    public @ResponseBody Iterable<ResearchSubject> getAllPractioner(){
        return researchSubjectRepository.findAll();
    }

    //Get ResearchSubject by ID
    @GetMapping("/{id}")
    public ResponseEntity <ResearchSubject> getResearchSubject(@PathVariable String id){
        return researchSubjectRepository
                .findById(id)
                .map(researchSubject -> ResponseEntity.ok().body(researchSubject))
                .orElse(ResponseEntity.notFound().build());
    }


    //https://spring.io/guides/tutorials/rest/

    @PostMapping()
    public ResponseEntity<ResearchSubject> createResearchSubject(@Valid @RequestBody ResearchSubject researchSubject) {
        var saved = researchSubjectRepository.save(researchSubject);
        return ResponseEntity.created(URI.create("api/researchSubject/" + saved.getId())).body(saved);
    }

    //Delete an ResearchSubject
    @DeleteMapping("/{id}")
    public ResponseEntity<ResearchSubject> deleteResearchSubject(@PathVariable(value = "id") String researchSubjectId) {
        return researchSubjectRepository
                .findById(researchSubjectId)
                .map(
                        researchSubject -> {
                            researchSubjectRepository.delete(researchSubject);
                            return ResponseEntity.ok().<ResearchSubject>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResearchSubject> updateResearchSubject(@PathVariable(value = "id") String id, @Valid @RequestBody ResearchSubject researchSubjectDetails) {
        return researchSubjectRepository.
                findById(id)
                .map(researchSubject ->{
                 researchSubject.setIdentifier(researchSubjectDetails.getIdentifier());
                 researchSubject.setStatus(researchSubjectDetails.getStatus());
                 researchSubject.setPeriod(researchSubjectDetails.getPeriod());
                 researchSubject.setIndividualization(researchSubjectDetails.getIndividualization());
                    researchSubject.setRs_datetime(researchSubjectDetails.getRs_datetime());
                            ResearchSubject updatedResearchSubject = researchSubjectRepository.save(researchSubject);
                            return ResponseEntity.ok(updatedResearchSubject);
                        }
                ).orElseGet(() -> createResearchSubject(researchSubjectDetails)); //
    }

}
