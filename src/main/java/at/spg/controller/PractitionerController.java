package at.spg.controller;

import at.spg.model.Practitioner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import at.spg.repositories.PractitionerRepository;

import javax.validation.Valid;
import java.net.URI;

@RequestMapping(path="/api/practitioner")
@RestController //REST: beschreibt Mechanismus dass daten über http ausgetauscht werden
@CrossOrigin(origins = "http://localhost:4200")
//stateless: requests unabhängig von vorherigen requests
public class PractitionerController {
 	@Autowired //singleton: dass es nur eine Instanz davon gibt  und  =new Practio...
    private PractitionerRepository practitionerRepository;

    @GetMapping
    public @ResponseBody Iterable<Practitioner> getAllPractioner(){
        return practitionerRepository.findAll();
    }

    //Get Practitioner by ID
    @GetMapping("/{id}")
    public ResponseEntity <Practitioner> getPractitioner(@PathVariable String id){
        return practitionerRepository
                .findById(id)
                .map(practitioner -> ResponseEntity.ok().body(practitioner))
                .orElse(ResponseEntity.notFound().build());
    }


    //https://spring.io/guides/tutorials/rest/

    @PostMapping()
    public ResponseEntity<Practitioner> createPractitioner(@Valid @RequestBody Practitioner practitioner) {
        var saved = practitionerRepository.save(practitioner);
        return ResponseEntity.created(URI.create("api/practitioner/" + saved.getId())).body(saved);
    }

    //Delete an Practitioner
    @DeleteMapping("/{id}")
    public ResponseEntity<Practitioner> deletePractitioner(@PathVariable(value = "id") String practitionerId) {
        return practitionerRepository
                .findById(practitionerId)
                .map(
                        practitioner -> {
                            practitionerRepository.delete(practitioner);
                            return ResponseEntity.ok().<Practitioner>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Practitioner> updatePractitioner(@PathVariable(value = "id") String id, @Valid @RequestBody Practitioner practitionerDetails) {
        return practitionerRepository.
                findById(id)
                .map(practitioner ->{
                 practitioner.setName(practitionerDetails.getName());
                 practitioner.setTelecom(practitionerDetails.getTelecom());
                 practitioner.setPhoto(practitionerDetails.getPhoto());
                 practitioner.setActive(practitionerDetails.isActive());
                 practitioner.setGender(practitionerDetails.getGender());
                 practitioner.setAddress(practitionerDetails.getAddress());
                 practitioner.setIdentifier(practitionerDetails.getIdentifier());
                 practitioner.setCommunication(practitionerDetails.getCommunication());
                 practitioner.setQualification(practitionerDetails.getQualification());
                 practitioner.setBirthDate(practitionerDetails.getBirthDate());
                            Practitioner updatedPractitioner = practitionerRepository.save(practitioner);
                            return ResponseEntity.ok(updatedPractitioner);
                        }
                ).orElseGet(() -> createPractitioner(practitionerDetails)); //
    }

}
