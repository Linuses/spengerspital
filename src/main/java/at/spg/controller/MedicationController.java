package at.spg.controller;


import at.spg.model.Medication;
import at.spg.model.Medication;
import at.spg.repositories.MedicationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.NoSuchElementException;

@RestController
@RequestMapping(path="/api/medication")
public class MedicationController {
    @Autowired
    private MedicationRepository medicationRepository;

    //Get all Medications
    @GetMapping
    public @ResponseBody Iterable<Medication> getAllMedications() {
       return medicationRepository.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity <Medication> getMedication(@PathVariable String id){
        return medicationRepository
                .findById(id)
                .map(medication -> ResponseEntity.ok().body(medication))
                .orElse(ResponseEntity.notFound().build());
    }


    @PostMapping()
    public ResponseEntity<Medication> createMedication(@Valid @RequestBody Medication medication) {
        if(medication.getIngredients().iterator().next().getItemReference() != null &&
                medication.getIngredients().iterator().next().getItemCodeableConcept() != null) {
            throw new IllegalArgumentException("Controller: itemCodeableConcept and itemReference cannot be set at the same time");
        }
        var saved = medicationRepository.save(medication);
        return ResponseEntity.created(URI.create("api/medication/" + saved.getId())).body(saved);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Medication> deleteMedication(@PathVariable(value = "id") String medicationId) {
        return medicationRepository
                .findById(medicationId)
                .map(
                        medication -> {
                            medicationRepository.delete(medication);
                            return ResponseEntity.ok().<Medication>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }


    @PutMapping("/{id}")
    public ResponseEntity<Medication> updateMedication(@PathVariable(value = "id") String id, @Valid @RequestBody Medication medicationDetails) {
        return medicationRepository.
                findById(id)
                .map(medication ->{

                    medication.setIdentifier(medicationDetails.getIdentifier());
                    medication.setCode(medicationDetails.getCode());
                    medication.setStatus(medicationDetails.getStatus());
                    medication.setManufacterer(medicationDetails.getManufacterer());
                    medication.setForm(medicationDetails.getForm());
                    medication.setAmount(medicationDetails.getAmount());
                    medication.setIngredients(medicationDetails.getIngredients());

                            Medication updatedMedication = medicationRepository.save(medication);
                            return ResponseEntity.ok(updatedMedication);
                        }
                ).orElseGet(() -> createMedication(medicationDetails)); //
    }
}

