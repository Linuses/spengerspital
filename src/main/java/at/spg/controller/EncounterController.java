package at.spg.controller;

import java.net.URI;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicLong;

import at.spg.model.Encounter;
import at.spg.model.Patient;
import at.spg.repositories.EncounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import at.spg.model.Encounter;
import at.spg.repositories.EncounterRepository;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping(path="/api/encounter") // This means URL's start with /demo (after Application path)
public class EncounterController {

    @Autowired
    private EncounterRepository encounterRepository;

    //Get all Encounters
    @GetMapping
    public @ResponseBody Iterable<Encounter> getAllEncounters(){
        // This returns a JSON or a XML
        return encounterRepository.findAll();
    }

    //Get Encounter by ID
    @GetMapping("/{id}")
    public ResponseEntity <Encounter> getEncounter(@PathVariable String id){
        return encounterRepository
                .findById(id)
                .map(encounter -> ResponseEntity.ok().body(encounter))
                .orElse(ResponseEntity.notFound().build());
    }

    @PostMapping()
    public ResponseEntity<Encounter> createEncounter(@Valid @RequestBody Encounter encounter) {
        var saved = encounterRepository.save(encounter);
        return ResponseEntity.created(URI.create("api/encounter/" + saved.getId())).body(saved);
    }

    //Delete an Encounter
    @DeleteMapping("/{id}")
    public ResponseEntity<Encounter> deleteEncounter(@PathVariable(value = "id") String encounterId) {
        return encounterRepository
                .findById(encounterId)
                .map(
                        encounter -> {
                            encounterRepository.delete(encounter);
                            return ResponseEntity.ok().<Encounter>build();
                        }
                ).orElse(ResponseEntity.notFound().build());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Encounter> updateEncounter(@PathVariable(value = "id") String id, @Valid @RequestBody Encounter encounterDetails) {
        return encounterRepository.
                findById(id)
                .map(encounter ->{
                    encounter.setIdentifier(encounterDetails.getIdentifier());
                    encounter.setStatus(encounterDetails.getStatus());
                    encounter.setStatusHistory(encounterDetails.getStatusHistory());
                    encounter.setType(encounterDetails.getType());
                    encounter.setClassfromenc(encounterDetails.getClassfromenc());
                    encounter.setSubject(encounterDetails.getSubject());
                    encounter.setEpisodeOfCare(encounterDetails.getEpisodeOfCare());
                    encounter.setParticipant(encounterDetails.getParticipant());
                    encounter.setAppointment(encounterDetails.getAppointment());
                    encounter.setPeriod(encounterDetails.getPeriod());
                    encounter.setDiagnosis(encounterDetails.getDiagnosis());
                    encounter.setPartOf(encounterDetails.getPartOf());

                            Encounter updatedEncounter = encounterRepository.save(encounter);
                            return ResponseEntity.ok(updatedEncounter);
                        }
                ).orElseGet(() -> createEncounter(encounterDetails)); //
    }
}