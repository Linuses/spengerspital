--um mariadb zu starten: https://wiki.archlinux.org/index.php/MariaDB

-- nicht alle FOREIGN KEYS pro klasse befüllen, immer nur einen

INSERT INTO `patient` (`id`, `p_active`, `p_birthdate`, `p_gender`) VALUES ('Test1', 1, '2000-03-03', 'female');
INSERT INTO `patient` (`id`, `p_active`, `p_birthdate`, `p_gender`) VALUES ('Test2', 1, '2000-02-02', 'male');
INSERT INTO `patient` (`id`, `p_active`, `p_birthdate`, `p_gender`) VALUES ('reference_patient', 1, '2000-02-02', 'male');

--INSERT INTO `practitioner` (`id`, `prac_active`, `p_birthdate`, `prac_gender`,`prac_general_practitioner_p_fk`) VALUES ('delete_this_practitioner', 0, '2000-01-02', 'male','reference_patient'');
INSERT INTO `practitioner` (`id`, `prac_active`, `p_birthdate`, `prac_gender`) VALUES ('33', 0, '2000-01-02', 'male');
INSERT INTO `practitioner` (`id`, `prac_active`, `p_birthdate`, `prac_gender`) VALUES ('66', 1, '1990-01-01', 'female');
INSERT INTO `practitioner` (`id`, `prac_active`, `p_birthdate`, `prac_gender`) VALUES ('666', 1, '1990-01-01', 'female');

INSERT INTO `codeable_concept` (`id`, `cc_text`) VALUES ('123123', 'Test');
INSERT INTO `codeable_concept` (`id`, `cc_text`) VALUES ('321321', 'Text');
INSERT INTO `codeable_concept` (`id`, `cc_text`) VALUES ('cc-notpatient', 'Text');
INSERT INTO `codeable_concept` (`id`, `cc_text`) VALUES ('ccfortest1', 'Text');
INSERT INTO `codeable_concept` (`id`, `cc_text`) VALUES ('ccfortest2', 'Text');

INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`) VALUES ('230', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi1412312');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_p_fk`, `i_codeableconcept_fk`) VALUES ('233', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi2','Test1','321321');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_p_fk`,`i_codeableconcept_fk`) VALUES ('239', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi1','Test1','123123');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_prac_fk`) VALUES ('idi-prac-33', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi33','33');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_prac_fk`) VALUES ('idi-prac-66', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi66','66');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_prac_fk`) VALUES ('idi-prac-666', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi666','666');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_prac_fk`) VALUES ('idi-prac-6662', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi6662','666');
INSERT INTO `address` (`id`, `add_city`, `add_country`, `add_district`, `pe_end`, `pe_start`, `add_postal_code`, `add_state`, `add_text`, `add_type`, `add_use`, `addre_p_fk`) VALUES ('address1', 'vienna', 'vienna', 'penzing', '2020-01-01', '2002-01-01', '1140', 'vienna', 'text', 'postal', 'home','Test1');
INSERT INTO `address` (`id`, `add_city`, `add_country`, `add_district`, `pe_end`, `pe_start`, `add_postal_code`, `add_state`, `add_text`, `add_type`, `add_use`,`addr_prac_fk`) VALUES ('address2', 'vienna', 'vienna', 'penzing', '2020-01-01', '2002-01-01', '1140', 'vienna', 'text', 'postal', 'home','33');
INSERT INTO `contact_point` (`id`, `ctp_value`, `pe_end`, `pe_start`, `ctp_rank`, `ctp_system`, `ctp_use`,`ctp_p_fk`) VALUES ('ctp1', 'veryNiceCTP', '2020-01-01', '2000-01-01', '1', 'other', 'home',  'Test1');
INSERT INTO `contact_point` (`id`, `ctp_value`, `pe_end`, `pe_start`, `ctp_rank`, `ctp_system`, `ctp_use`) VALUES ('ctp0', 'veryNiceCTP', '2020-01-01', '2000-01-01', '1', 'other', 'home');
INSERT INTO `contact_point` (`id`, `ctp_value`, `pe_end`, `pe_start`, `ctp_rank`, `ctp_system`, `ctp_use`, `ctp_prac_fk`) VALUES ('ctp2', 'veryNiceCTP', '2020-01-01', '2000-01-01', '1', 'other', 'home', '33');
INSERT INTO `attachment` (`id`, `atta_creation`, `atta_data`, `atta_hash`, `atta_language`, `atta_size`, `atta_title`, `atta_url`,  `att_p_fk`) VALUES ('atta1', '2002-03-29', 'DATA', 'e44f9e348e41cb272efa87387728571b', 'de', '132456', 'AttachmentTitle', 'url', 'Test1');
INSERT INTO `attachment` (`id`, `atta_creation`, `atta_data`, `atta_hash`, `atta_language`, `atta_size`, `atta_title`, `atta_url`, `attaphoto_prac_fk`) VALUES ('atta2', '2002-03-29', 'DATA', 'e44f9e348e41cb272efa87387728571b', 'de', '132456', 'AttachmentTitle', 'url', '33');

INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`) VALUES ('reference2', 'display', 'referencedSomething');
-- insert into `coding` (id) VALUES (1); --für den fk von encounter
INSERT INTO `human_name` (`id`, `pe_end`, `pe_start`, `hun_text`, `hun_use`, `name_prac_fk`,`hun_family`) VALUES ('name23', '2000-01-06', '2000-01-02', 'HubertMueller', 'usual', '33','LabelFamilyMueller');
INSERT INTO `human_name` (`id`, `pe_end`, `pe_start`, `hun_text`, `hun_use`, `name_prac_fk`,`hun_family`) VALUES ('name223', '2000-01-06', '2000-01-02', 'HubertMueller2', 'usual', '33','LabelFamilyMueller');
INSERT INTO `human_name` (`id`, `pe_end`, `pe_start`, `hun_text`, `hun_use`, `name_prac_fk`,`hun_family`) VALUES ('nameTest2', '2000-01-06', '2000-01-02', 'Manfred Herst', 'usual', '66','LabelFamilyAasdf');
INSERT INTO `human_name` (`id`, `pe_end`, `pe_start`, `hun_text`, `hun_use`, `name_p_fk`,`hun_family`) VALUES ('name33', '2000-01-06', '2000-01-02', 'Raginhard', 'usual','Test1','asdfpoiuqwerlabel');
INSERT INTO `human_name` (`id`, `pe_end`, `pe_start`, `hun_text`, `hun_use`, `name_p_fk`,`hun_family`) VALUES ('name33_2', '2000-01-06', '2000-01-02', 'Raginhard2', 'nickname','Test1','familynamelabel');
INSERT INTO `human_name` (`id`, `pe_end`, `pe_start`, `hun_text`, `hun_use`, `name_p_fk`,`hun_family`) VALUES ('nameTest3', '2000-01-06', '2000-01-02', 'Isgard Adalwin', 'usual', 'Test2','LabelFamilyMaier');

INSERT INTO `encounter` (`id`, `pe_end`, `pe_start`, `enc_status`, `enc_subject_fk`) VALUES ('encounter1', '2020-01-13', '2020-01-01', 'onleave', 'reference2');
INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`, `ref_type`, `identifier_id`, `ref_epi_of_care_enc_fk`) VALUES ('reference3', 'display', 'referencedSomething', null, '230', 'encounter1');
INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`, `ref_type`, `identifier_id`, `ref_appointment_fk`) VALUES ('reference4', 'display', 'referencedSomething', null, '230',  'encounter1');
INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`, `ref_type`, `identifier_id`) VALUES ('reference5', 'display', 'localhost:4200/practitioners/33', null, '230');
INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`, `ref_type`, `identifier_id`) VALUES ('reference6', 'display', 'localhost:4200/patients/Test1', null, '230');
INSERT INTO `participant` (`id`, `pe_end`, `pe_start`, `participant_individual_fk`) VALUES ('participant1', '2000-02-02 00:00:00.000000', '1999-02-02 00:00:00.000000', 'reference2');
INSERT INTO `participant` (`id`, `pe_end`, `pe_start`, `participant_enc_fk`) VALUES ('participant2', '2000-02-02', '2000-01-29',  'encounter1');

INSERT INTO `encounter` (`id`, `pe_end`, `pe_start`, `enc_status`, `enc_subject_fk`) VALUES ('encounter2', '2020-01-13', '2020-01-01', 'onleave', 'reference2');
INSERT INTO `encounter` (`id`, `pe_end`, `pe_start`, `enc_status`, `enc_subject_fk`) VALUES ('encounter3', '2020-01-13', '2020-01-01', 'onleave', 'reference2');
INSERT INTO `encounter` (`id`, `pe_end`, `pe_start`, `enc_status`,  `enc_part_of_fk`) VALUES ('encounter4', '2020-01-13', '2020-01-01', 'onleave',  'reference2');

INSERT INTO `qualification` (`id`, `pe_end`, `pe_start`, `qualific_code_fk`) VALUES ('11', '2002-01-01', '2002-01-01', 'cc-notpatient');
INSERT INTO `qualification` (`id`, `pe_end`, `pe_start`, `qual_prac_fk`) VALUES ('12', '2002-01-01', '2002-01-01',  '33');
INSERT INTO `qualification` (`id`, `pe_end`, `pe_start`, `qual_prac_fk`) VALUES ('16', '2002-01-01', '2002-01-01',  '666');
INSERT INTO `qualification` (`id`, `pe_end`, `pe_start`, `qual_prac_fk`) VALUES ('166', '2002-01-01', '2002-01-01',  '66');
INSERT INTO `qualification` (`id`, `pe_end`, `pe_start`, `qual_prac_fk`) VALUES ('167', '2002-01-01', '2002-01-01',  '66');

INSERT INTO `quantity` (`id`, `quant_code_fk`, `quant_comperator_fk`, `quant_unit_fk`, `quant_uri_fk`, `quant_value_fk`) VALUES ('quantity1', 'qcode', 'lessthan', 'qunit', 'docs/guide/collections', '66.6');
INSERT INTO `ratio` (`id`, `ratio_denominator_fk`, `ratio_numerator_fk`) VALUES ('ratio1', 'quantity1', 'quantity1');
INSERT INTO `batch` (`id`, `batch_expiration_date`, `batch_lot_number`) VALUES ('batch1', '2000-02-02', 'batchnumber');

INSERT INTO `medication` (`id`, `med_manufacterer_fk`) VALUES ('med1',  'reference2');
INSERT INTO `medication` (`id`,  `med_code_fk`) VALUES ('med2', '321321');
INSERT INTO `medication` (`id`,  `med_batch_fk`) VALUES ('med3', 'batch1');
INSERT INTO `medication` (`id`, `med_amount_fk`) VALUES ('med4',  'ratio1');
INSERT INTO `medication` (`id`, `med_status_fk`) VALUES ('med5', 'active');
INSERT INTO `medication` (`id`, `med_form_fk`) VALUES ('med6', '321321');

INSERT INTO `ingredient` (`id`, `ingredient_isactive`, `ingredient_item_codeable_concept`,  `ingredient_strength`,`ingredient_med_fk`) VALUES ('ingre1', TRUE, '321321',  'ratio1','med1');
INSERT INTO `ingredient` (`id`, `ingredient_isactive`, `ingredient_item_codeable_concept`, `ingredient_item_reference_fk`, `ingredient_strength`) VALUES ('ingre2', TRUE, '321321', 'reference2', 'ratio1');

INSERT INTO `device_use_statement` (`id`, `device_use_statement_recorded_date`, `device_use_statement_stats`,`device_timing_often_date_time`,`pe_end`,`pe_start`) VALUES ('deviceUseStatement1', '2020-01-01', 'acting','2020-03-01','2020-03-01','2020-01-01');

INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_codeableconcept_fk`) VALUES ('236', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi3','ccfortest1');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,  `i_p_fk`) VALUES ('234', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi4','Test2');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_codeableconcept_fk`) VALUES ('235', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi5','ccfortest2');
--INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_codeableconcept_fk`, `i_p_fk`,`i_qual_fk`,`i_prac_fk`,`med_identifier_fk`,`i_enc_fk`) VALUES ('23333', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi2','123123','Test1','11','66','med1','encounter1');
--INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_codeableconcept_fk`, `i_p_fk`,`i_qual_fk`,`i_prac_fk`,`med_identifier_fk`,`i_enc_fk`) VALUES ('235', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi22','cc-notpatient','Test1','11','33','med1','encounter1');

INSERT INTO `flag` (`id`, `pe_end`, `pe_start`, `flag_status`, `flag_reference`) VALUES ('flag1', '2002-01-01 00:00:00.000000', '2001-01-01 00:00:00.000000', 'between', 'reference5');
INSERT INTO `flag` (`id`, `pe_end`, `pe_start`, `flag_status`, `flag_reference`) VALUES ('flag2', '2018-01-01 00:00:00.000000', '2009-01-01 00:00:00.000000', 'all', 'reference6');

INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_flag_fk`) VALUES ('idi-flag-66', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi66','flag1');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_flag_fk`) VALUES ('idi-flag-61', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi66asdfd','flag1');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`,`i_flag_fk`) VALUES ('idi-flag-63', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi663','flag2');

-- PractitionerRole

INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`, `ref_type`) VALUES ('reference55', 'display', 'localhost:4200/practitioners/33', null);

INSERT INTO `reference` (`id`, `ref_display`, `ref_reference`, `ref_type`) VALUES ('reference56', 'display', 'localhost:4200/practitioners/33', null);

INSERT INTO `practitioner_role` (`id`, `prac_role_active`, `pe_end`, `pe_start`, `prac_role_reference`) VALUES ('prole1', True, '2020-01-01 00:00:00.000000', '2002-01-01 00:00:00.000000', 'reference55');
INSERT INTO `practitioner_role` (`id`, `prac_role_active`, `pe_end`, `pe_start`, `prac_role_reference`) VALUES ('prole2', True, '2020-01-01 00:00:00.000000', '2002-01-01 00:00:00.000000', 'reference56');

-- codings
INSERT INTO `codeable_concept` (`id`, `cc_prac_role_fk`) VALUES ('cc_coding_prole1', 'prole1');
INSERT INTO `codeable_concept` (`id`, `cc_prac_role_fk`) VALUES ('cc_coding_prole2', 'prole2');

INSERT INTO `coding` (`id`, `c_code`, `c_display`, `c_system`) VALUES ('408467006', '408467006', 'Adult mental illness', 'https://browser.ihtsdotools.org/?perspective=full&conceptId1=408467006&edition=MAIN/2021-01-31&release=&languages=en');
INSERT INTO `coding` (`id`, `c_code`, `c_display`, `c_system`) VALUES ('394577000', '394577000', 'Anesthetics', 'https://browser.ihtsdotools.org/?perspective=full&conceptId1=394577000');
UPDATE `coding` SET `coding_codeableconcept_fk` = 'cc_coding_prole1' WHERE (`id` = '394577000');
UPDATE `coding` SET `coding_codeableconcept_fk` = 'cc_coding_prole2' WHERE (`id` = '408467006');
UPDATE `coding` SET `c_user_selected` = True WHERE (`id` = '394577000');
UPDATE `coding` SET `c_user_selected` = False WHERE (`id` = '408467006');



INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_prac_role_fk`) VALUES ('23333', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi1pracrole2', 'prole2');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_prac_role_fk`) VALUES ('23333_2', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi1pracrole2_2', 'prole2');
INSERT INTO `identifier` (`id`, `i_code`, `pe_end`, `pe_start`, `i_system`, `i_value`, `i_prac_role_fk`) VALUES ('23334', 'secondary', '2020-01-01', '2000-01-01', 'http://url', 'idi1pracrole1', 'prole1');

INSERT INTO `practitioner_role_available_time` (`id`, `prac_role_at_all_day`, `prac_role_at_available_end_time`, `prac_role_at_available_start_time`, `bbe_prac_role_fk`) VALUES ('187', True, '2021-01-01 00:00:00.000000', '2002-01-01 00:00:00.000000', 'prole1');
INSERT INTO `practitioner_role_available_time` (`id`, `prac_role_at_all_day`, `prac_role_at_available_end_time`, `prac_role_at_available_start_time`, `bbe_prac_role_fk`) VALUES ('1877', True, '2022-01-01 00:00:00.000000', '2002-02-01 00:00:00.000000', 'prole2');
INSERT INTO `practitioner_role_available_time` (`id`, `prac_role_at_all_day`, `prac_role_at_available_end_time`, `prac_role_at_available_start_time`, `bbe_prac_role_fk`) VALUES ('1872', False, '2023-01-01 00:00:00.000000', '2002-03-01 00:00:00.000000', 'prole1');
INSERT INTO `practitioner_role_available_time` (`id`, `prac_role_at_all_day`, `prac_role_at_available_end_time`, `prac_role_at_available_start_time`, `bbe_prac_role_fk`) VALUES ('18772', False, '2024-01-01 00:00:00.000000', '2002-04-01 00:00:00.000000', 'prole2');

-- TODO: how to insert into @Elementcollection
INSERT INTO  `days_of_week` (`id`, `prac_role_at_days_of_week`) VALUES (187, 'tue');
INSERT INTO  `days_of_week` (`id`, `prac_role_at_days_of_week`) VALUES (1877, 'mon');
INSERT INTO  `days_of_week` (`id`, `prac_role_at_days_of_week`) VALUES (1877, 'sat');


-- rs
INSERT INTO  `identifier` (`id`, `i_code`, `i_value`) VALUES ('idi-rs1', 'secondary', 'idi123123123123');
INSERT INTO  `identifier` (`id`, `i_code`, `i_value`) VALUES ('idi-rs2', 'secondary', 'idi12312312312312312');

INSERT INTO  `reference` (`id`, `ref_display`, `ref_reference`) VALUES ('reference123123', 'display', 'localhost:4200/patients/Test1');
INSERT INTO  `reference` (`id`, `ref_display`, `ref_reference`) VALUES ('reference123123123', 'display', 'https://www.bing.com/');
INSERT INTO  `research_subject` (`id`, `pe_end`, `pe_start`, `rs_datetime`, `rs_status`, `rs_identifier`, `rs_individualization`) VALUES ('rs1', '2021-01-01 00:00:00.000000', '2021-01-01 00:00:00.000000', '2021-01-01 00:00:00.000000', 'candit', 'idi-rs1', 'reference123123');
INSERT INTO  `research_subject` (`id`, `pe_end`, `pe_start`, `rs_datetime`, `rs_status`, `rs_identifier`, `rs_individualization`) VALUES ('rs2', '2021-01-01 00:00:00.000000', '2021-01-01 00:00:00.000000', '2021-01-01 00:00:00.000000', 'candit', 'idi-rs2', 'reference123123123');
