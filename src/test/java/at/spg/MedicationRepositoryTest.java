package at.spg;

import at.spg.model.*;
import at.spg.repositories.MedicationRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class MedicationRepositoryTest {

    @Autowired
    MedicationRepository medicationRepository;

    @Test
    @Transactional
    public void testSaveAMediaction() {

        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)
        );
        identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));
        Identifier oneidentifier = new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);

        Batch batch = new Batch("asdf",LocalDate.of(2000, 01, 01));
        String uri = ("http://localhost:8080/customer/");
        Reference manufacterer= new Reference("reference",uri,oneidentifier,"display");
        Quantity quantity = new Quantity(Quantity.QuantityComparator.lessorequal, (float) 6.6,"unit","www.google.com","code123");
        Ratio amount = new Ratio(quantity,quantity);
        Set<Ingredient> ingredients= new HashSet<>();
        boolean isActive = false;
        Ingredient ingrre = new Ingredient(ccType,isActive,amount);
        ingrre.setItemReference(manufacterer);
//        ingrre.setItemCodeableConcept(ccType);  //look, if the you can set both items arguments at the same time
        ingredients.add(new Ingredient(ccType,isActive,amount));


        Medication m = new Medication(identifier,ccType, Medication.StatusCode.active, manufacterer,ccType,amount,ingredients,batch);
        Medication retMed = medicationRepository.save(m);
        Medication findMed = medicationRepository.findById(retMed.getId()).get();

        assertTrue(CollectionUtils.isEqualCollection(retMed.getIdentifier(), findMed.getIdentifier()));
        assertEquals(retMed.getCode(), findMed.getCode());
        assertEquals(retMed.getStatus(),findMed.getStatus());
        assertEquals(retMed.getManufacterer(),findMed.getManufacterer());
        assertEquals(retMed.getForm(),findMed.getForm());
        assertEquals(retMed.getAmount(),findMed.getAmount());
        assertTrue(CollectionUtils.isEqualCollection(retMed.getIngredients(),findMed.getIngredients()));
        assertEquals(retMed.getBatch(),findMed.getBatch());
    }
}
