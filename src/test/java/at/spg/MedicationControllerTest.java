package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
@SpringBootTest
@AutoConfigureMockMvc
public class MedicationControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    @Transactional
    public void getAllMedication() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/medication")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void getPatient() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/medication/med1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void postPatient() throws Exception { //create
        Medication m = returnFullmedication();
        m.setId(null);
        var json = om.writeValueAsString(m); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/medication")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putMedicationNotFound() throws Exception { //update
        Medication e = returnFullmedication();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/medication/Encounter1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putPatient() throws Exception { //update
        Medication m = returnFullmedication();
        var json = om.writeValueAsString(m); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/medication/med1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void deleteMedication() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/medication/med1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public Medication returnFullmedication() {
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)
        );
        identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));
        Identifier oneidentifier = new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);

        Batch batch = new Batch("asdf", LocalDate.of(2000, 01, 01));
        String uri = ("http://localhost:8080/irgendwas/");
        Reference manufacterer= new Reference("reference",uri,oneidentifier,"display");
        Quantity quantity = new Quantity(Quantity.QuantityComparator.lessorequal, (float) 6.6,"unit","www.google.com","code123");
        Ratio amount = new Ratio(quantity,quantity);
        Set<Ingredient> ingredients= new HashSet<>();
        boolean isActive = false;
        Ingredient ingrre = new Ingredient(ccType,isActive,amount);
        ingrre.setItemReference(manufacterer);
//        ingrre.setItemCodeableConcept(ccType);  //look, if the you can set both items arguments at the same time
        ingredients.add(new Ingredient(ccType,isActive,amount));

        Medication m = new Medication(identifier,ccType, Medication.StatusCode.active, manufacterer,ccType,amount,ingredients,batch);
        return m;
    }
}
