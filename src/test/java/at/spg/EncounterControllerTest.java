package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
public class EncounterControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    @Transactional
    public void getAllEncounter() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/encounter")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void getEncounter() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/encounter/encounter2"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void postEncounter() throws Exception { //create
        Encounter e = returnFullencounter();
        e.setId(null);
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/encounter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putEncounterNotFound() throws Exception { //update
        Encounter e = returnFullencounter();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/encounter/Encounter1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }


    @SneakyThrows
    @Test
    @Transactional
    public void putEncounter() throws Exception { //update
        Encounter e = returnFullencounter();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/encounter/encounter2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void deleteEncounter() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/encounter/encounter2"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public Encounter returnFullencounter() {
        Set<Identifier> identifier = new HashSet<>();
        Coding classfromenc = new Coding();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        Period period = new Period(
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)
        );
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Set<CodeableConcept> type = new HashSet<>();
        type.add(ccType);
        identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));
        Set<Statushistory> statusHistory = new HashSet<>();
        Reference subject = new Reference();
        Reference partOf = new Reference();
        Set<Reference> episodeOfCare= new HashSet<>();
        Set<Reference> appointment= new HashSet<>();
        Set<Diagnosis> diagnoses = new HashSet<>();

        Set<Participant> participant = new HashSet<>();

        Encounter e = new Encounter(identifier,Encounter.StatusCode.planned,statusHistory,type,classfromenc,subject,
                episodeOfCare,participant, appointment,period,diagnoses,partOf);
        return e;
    }
}
