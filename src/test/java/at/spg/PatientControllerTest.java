package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@SpringBootTest
@AutoConfigureMockMvc
public class PatientControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    public void getAllPatient() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/api/patient")
                .contentType(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    public void getPatient() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/api/patient/Test1"))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
    }



    @SneakyThrows
    @Test
    public void postPatient() throws Exception { //create
        Patient p = returnFullpatient();
        p.setId(null);
        var json = om.writeValueAsString(p); //to generate an JSON from the patient

        mockMvc
            .perform(MockMvcRequestBuilders.post("/api/patient")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                //manuell:
                //.content("{\"svnr\": \"33\"}"))
                //machinell:
                .content(json))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putPatientNotFound() throws Exception { //update
        Patient e = returnFullpatient();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/patient/Encounter1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    public void putPatient() throws Exception { //update
        Patient p = returnFullpatient();
        var json = om.writeValueAsString(p); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/patient/Test1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        //manuell:
                        //.content("{\"svnr\": \"33\"}"))
                        //machinell:
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    public void deletePatient() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/patient/Test2"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public Patient returnFullpatient() {
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        Period period = new Period(
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)
        );
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));


        Set<ContactPoint> telecom = new HashSet<>();

        telecom.add(new ContactPoint("Value",ContactPoint.SystemCode.other,ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));

        Set<Address> address = new HashSet<>();
        Set<String> line = new HashSet<>();
        line.add("sfdakjfsad");
        address.add(new Address(Address.UseCode.home,Address.TypeCode.both,"text",line,"vienna",
                "14","vienna","1140","austria",period
        ));

        Set<Attachment> photo = new HashSet<>();
        Long size = 12345L;
        String url ="null";
        photo.add(new Attachment(Attachment.ContentTypeCode.urn,Attachment.LanguageCode.de,"data",
                url,size,"hash",
                "title",LocalDateTime.of(2000,01,01,1,1)));

        String uri = ("http://localhost:8080/customer/");
        Identifier identi=new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
        Reference ref = new Reference("reference",uri,identi,"display");
        Set<Practitioner> generalPractitioner = new HashSet<>();
        generalPractitioner.add(returnFullPractitioner());

        Set<HumanName> humanName = new HashSet<>();
        Set<String> given = new HashSet<>();
        given.add("asdfsfa");
        HumanName humanName1 = new HumanName("text", HumanName.UseCode.anonymous,given,given,given,period,"LabelFamily");

        Patient p = new Patient(true, Patient.GenderCode.female,
                LocalDate.of(2000, 01, 01),identifier,humanName,
                telecom, LocalDateTime.of(2000,01,01,1,1), address,
                photo,generalPractitioner);

        return p;
    }

    public Practitioner returnFullPractitioner() {
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)
        );
        identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));


        Set<HumanName> name = new HashSet<>();
        Set<String> given = new HashSet<>();
        Set<String> prefix = new HashSet<>();
        Set<String> suffix = new HashSet<>();
        name.add(new HumanName("text",HumanName.UseCode.anonymous,given,prefix,suffix,period,"labelfamily"));


        Set<ContactPoint> telecom = new HashSet<>();
        telecom.add(new ContactPoint("Value",ContactPoint.SystemCode.other,ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));

        Set<Address> address = new HashSet<>();
        Set<String> line = new HashSet<>();
        address.add(new Address(Address.UseCode.home,Address.TypeCode.both,"text",line,"vienna",
                "14","vienna","1140","austria",period
        ));

        Set<Attachment> photo = new HashSet<>();
        Long size = 12345L;
        String url ="null";
        photo.add(new Attachment(Attachment.ContentTypeCode.urn,Attachment.LanguageCode.de,"data",
                url,size,"hash",
                "title",LocalDateTime.of(2000,01,01,1,1)));

        Set<Qualification> qualification = new HashSet<>();
        Set<Extension> modifierExtension = new HashSet<>();
        CodeableConcept code = new CodeableConcept();
        qualification.add(new Qualification(modifierExtension,identifier,code,period));

        Set<CodeableConcept> communication = new HashSet<>();
        communication.add(new CodeableConcept(coding,"text"));

        Practitioner prac = new Practitioner(identifier, true, name,telecom,
                address,Practitioner.GenderCode.other,
                LocalDate.of(2000, 01, 01), photo, qualification, communication
        );
        return prac;
    }
}