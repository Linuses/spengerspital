package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
public class DeviceUseTestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    @Transactional
    public void getAllDeviceUseStatement() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/deviceUseStatement")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void getDeviceUseStatement() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/deviceUseStatement/deviceUseStatement1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void postDeviceUseStatement() throws Exception { //create
        DeviceUseStatement e = returnFulldeviceUseStatement();
        e.setId(null);
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/deviceUseStatement")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putDeviceUseStatementNotFound() throws Exception { //update
        DeviceUseStatement e = returnFulldeviceUseStatement();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/deviceUseStatement/Encounter1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putDeviceUseStatement() throws Exception { //update
        DeviceUseStatement e = returnFulldeviceUseStatement();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/deviceUseStatement/deviceUseStatement1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void deleteDeviceUseStatement() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/deviceUseStatement/deviceUseStatement1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public DeviceUseStatement returnFulldeviceUseStatement() {
        Set<Identifier> identifier = new HashSet<>();
        Coding classfromenc = new Coding();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
        Set<CodeableConcept> type = new HashSet<>();
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        type.add(ccType);
        Identifier identi=new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
        identifier.add(identi);
        String uri = ("http://localhost:8080/customer/");
        Reference ref = new Reference("reference",uri,identi,"display");
        Set<Reference> basedOn= new HashSet<>();
        basedOn.add(ref);
        DeviceTiming timing = new DeviceTiming(LocalDateTime.of(2000, 01,01,1,1),period);


        DeviceUseStatement e = new DeviceUseStatement(identifier,basedOn,DeviceUseStatement.StatusCode.acting,timing,LocalDateTime.of(2000, 01,01,1,1));
        return e;
    }
}
