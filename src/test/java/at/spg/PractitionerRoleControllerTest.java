package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
public class PractitionerRoleControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    @Transactional
    public void getAllPractitionerRole() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/practitionerRole")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void getPractitionerRole() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/practitionerRole/prole2"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void postPractitionerRole() throws Exception { //create
        PractitionerRole e = returnFullpractitionerRole();
        e.setId(null);
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/practitionerRole")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putPractitionerRoleNotFound() throws Exception { //update
        PractitionerRole e = returnFullpractitionerRole();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/practitionerRole/Encounter1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putPractitionerRole() throws Exception { //update
        PractitionerRole e = returnFullpractitionerRole();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/practitionerRole/prole2")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void deletePractitionerRole() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/practitionerRole/prole2"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public PractitionerRole returnFullpractitionerRole() {
        // 1. Erstellen einer PractitionerRoleInstanz
        Set<Identifier> identifier = new HashSet<>();
        Coding classfromenc = new Coding();
        Set<Coding> coding = new HashSet<>();
        //ich weiß genau was Sie verlangen, bei "In der Speciality sind folgende 2 Codes zu ermöglichen: "
        //reicht das?
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        coding.add(new Coding("https://browser.ihtsdotools.org/?perspective=full&conceptId1=394577000","0.1", "Adult mental illness", "408467006", false ));
        coding.add(new Coding("https://browser.ihtsdotools.org/?perspective=full&conceptId1=408467006&edition=MAIN/2021-01-31&release=&languages=en","0.1", "Anesthetics", "394577000", false));
        Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
        Set<CodeableConcept> type = new HashSet<>();
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        type.add(ccType);
        Identifier identi=new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
        identifier.add(identi);
        Set<Statushistory> statusHistory = new HashSet<>();
        Statushistory sHis = new Statushistory(Statushistory.StatusCode.onleave,period);
        statusHistory.add(sHis);
        Reference subject = new Reference();
        Reference partOf = new Reference();
        Set<Reference> episodeOfCare= new HashSet<>();
        String uri = ("http://localhost:8080/customer/");
        Reference ref = new Reference("practitioner",uri,identi,"display");
        Set<Reference> appointment= new HashSet<>();
        appointment.add(ref);
        Set<Diagnosis> diagnoses = new HashSet<>();
        Diagnosis diagnosis1 = new Diagnosis(ref,ccType,2);
        diagnoses.add(diagnosis1);
        Set<Participant> participant = new HashSet<>();
        Participant parti = new Participant(type,period,ref);
        participant.add(parti);





        Set<PractitionerRole_availableTime.DaysOfWeek> daysOfWeeks = new HashSet<>();
        daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.sat);
        daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.tue);
        daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.wed);
        daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.mon);

        Set<PractitionerRole_availableTime> practitionerRoleAvailableTime= new HashSet<>();
        practitionerRoleAvailableTime.add(new PractitionerRole_availableTime(
                daysOfWeeks, true,
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)));



        PractitionerRole e = new PractitionerRole(identifier, true,period,ref,type,practitionerRoleAvailableTime);

        return e;
    }
}
