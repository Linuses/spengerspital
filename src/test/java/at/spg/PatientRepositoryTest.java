package at.spg;

import java.net.URI;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import at.spg.model.*;
import org.junit.jupiter.api.Test;
import org.apache.commons.collections4.CollectionUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import at.spg.model.Patient.GenderCode;
import at.spg.repositories.PatientRepository;
import at.spg.model.Identifier.UseCode;


@SpringBootTest
public class PatientRepositoryTest {
	
	@Autowired //Instanzen auf unsere DB verwalten, dass man nicht unendlich viele DB Connections machen kann (man muss sich selber immer abmelden)
	PatientRepository patientRepository;
	
	@Test
	@Transactional //löscht auch die anderen, alles auf einmal
	public void testSaveOnePatient() {
		// 1. Erstellen einer PatientenInstanz
		
		Set<Identifier> identifier = new HashSet<>();
		Set<Coding> coding = new HashSet<>();
		coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
		Period period = new Period(
				LocalDateTime.of(2000, 01,01,1,1), 
				LocalDateTime.of(2000, 01,01,2,2)
			);
		CodeableConcept ccType = new CodeableConcept(coding, "Text");
		identifier.add(new Identifier(UseCode.official, "System","value",period ,ccType));
		
		
		Set<ContactPoint> telecom = new HashSet<>();
		
		telecom.add(new ContactPoint("Value",ContactPoint.SystemCode.other,ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));

		Set<Address> address = new HashSet<>();
		Set<String> line = new HashSet<>();
		line.add("sfdakjfsad");
		address.add(new Address(Address.UseCode.home,Address.TypeCode.both,"text",line,"vienna",
				"14","vienna","1140","austria",period
		));

		Set<Attachment> photo = new HashSet<>();
		Long size = 12345L;
		String url ="null";
		photo.add(new Attachment(Attachment.ContentTypeCode.urn,Attachment.LanguageCode.de,"data",
				url,size,"hash",
				"title",LocalDateTime.of(2000,01,01,1,1)));

		String uri = ("http://localhost:8080/customer/");
		Identifier identi=new Identifier(UseCode.official, "System","value",period ,ccType);
		Reference ref = new Reference("reference",uri,identi,"display");

		Set<HumanName> humanName = new HashSet<>();
		Set<String> given = new HashSet<>();
		given.add("asdfsfa");
		HumanName humanName1 = new HumanName("text", HumanName.UseCode.anonymous,given,given,given,period,"labelfamily");

		Set<Practitioner> generalPractitioner = new HashSet<>();
		generalPractitioner.add(returnFullPractitioner());

		Patient p = new Patient(true,GenderCode.female,
				LocalDate.of(2000, 01, 01),identifier,humanName,
				telecom, LocalDateTime.of(2000,01,01,1,1), address,
				photo,generalPractitioner);
		
		// 2. Instanz mit Testdaten befüllen
		
		//--- erledigt
		
		// 3. Instanz in die DB speichern
		
		Patient retPat = patientRepository.save(p);
		
		// 4. Gespeicherte Daten aus der DB lesen
		
		Patient findPat = patientRepository.findById(retPat.getId()).get();
		//Problematik ist, wir haben einen Patienten erstellt und erst 
		//nach dem save wird die automatisch erstellte nummer generiert
		
		//das get bruacht man weil ein optional zurückgeliefert wird und dadurch werden keine nullpointers zurückgeliefert
		
		// 5. Vergleich der gespeicherten Daten mit den geladenen 
		
		//man testet mit asserts (assertTrue: ob ein boolean wahr ist,...)
		//asserts sind praktische, weil wenn ein Assert nicht erfüllt ist, dann ist der Test fehlgeschlagen

		assertEquals(retPat.isActive(), findPat.isActive());
		assertEquals(retPat.getGender(), findPat.getGender());
		assertEquals(retPat.getBirthDate(), findPat.getBirthDate());
		assertTrue(CollectionUtils.isEqualCollection(retPat.getIdentifier(), findPat.getIdentifier()));
		assertTrue(CollectionUtils.isEqualCollection(retPat.getName(), findPat.getName()));
		assertTrue(CollectionUtils.isEqualCollection(retPat.getTelecom(), findPat.getTelecom()));
		assertEquals(retPat.getDeceasedDateTime(), findPat.getDeceasedDateTime());
		assertTrue(CollectionUtils.isEqualCollection(retPat.getAddress(), findPat.getAddress()));
		assertTrue(CollectionUtils.isEqualCollection(retPat.getPhoto(), findPat.getPhoto()));
		assertTrue(CollectionUtils.isEqualCollection(retPat.getGeneralPractitioner(), findPat.getGeneralPractitioner()));
	}

	public Practitioner returnFullPractitioner() {
		Set<Identifier> identifier = new HashSet<>();
		Set<Coding> coding = new HashSet<>();
		coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
		CodeableConcept ccType = new CodeableConcept(coding, "Text");
		Period period = new Period(
				LocalDateTime.of(2000, 01,01,1,1),
				LocalDateTime.of(2000, 01,01,2,2)
		);
		identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));


		Set<HumanName> name = new HashSet<>();
		Set<String> given = new HashSet<>();
		Set<String> prefix = new HashSet<>();
		Set<String> suffix = new HashSet<>();
		name.add(new HumanName("text",HumanName.UseCode.anonymous,given,prefix,suffix,period,"labelfamily"));


		Set<ContactPoint> telecom = new HashSet<>();
		telecom.add(new ContactPoint("Value",ContactPoint.SystemCode.other,ContactPoint.UseCode.home,0,
				new Period(LocalDateTime.of(2000,01,01,1,1),
						LocalDateTime.of(2000,01,01,1,1))));

		Set<Address> address = new HashSet<>();
		Set<String> line = new HashSet<>();
		address.add(new Address(Address.UseCode.home,Address.TypeCode.both,"text",line,"vienna",
				"14","vienna","1140","austria",period
		));

		Set<Attachment> photo = new HashSet<>();
		Long size = 12345L;
		String url ="null";
		photo.add(new Attachment(Attachment.ContentTypeCode.urn,Attachment.LanguageCode.de,"data",
				url,size,"hash",
				"title",LocalDateTime.of(2000,01,01,1,1)));

		Set<Qualification> qualification = new HashSet<>();
		Set<Extension> modifierExtension = new HashSet<>();
		CodeableConcept code = new CodeableConcept();
		qualification.add(new Qualification(modifierExtension,identifier,code,period));

		Set<CodeableConcept> communication = new HashSet<>();
		communication.add(new CodeableConcept(coding,"text"));

		Practitioner prac = new Practitioner(identifier, true, name,telecom,
				address,Practitioner.GenderCode.other,
				LocalDate.of(2000, 01, 01), photo, qualification, communication
		);
		return prac;
	}
}