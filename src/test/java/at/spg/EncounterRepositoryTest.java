package at.spg;

import at.spg.model.*;
import at.spg.model.Identifier.UseCode;
import at.spg.model.Encounter.StatusCode;
import at.spg.repositories.EncounterRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Condition;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class EncounterRepositoryTest {

	@Autowired //Instanzen auf unsere DB verwalten, dass man nicht unendlich viele DB Connections machen kann (man muss sich selber immer abmelden)
	EncounterRepository encounterRepository;

	@Test
	@Transactional
	public void testSaveOneEncounter() {
		// 1. Erstellen einer EncounterInstanz
		Set<Identifier> identifier = new HashSet<>();
		Coding classfromenc = new Coding();
		Set<Coding> coding = new HashSet<>();
		coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
		Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
		Set<CodeableConcept> type = new HashSet<>();
		CodeableConcept ccType = new CodeableConcept(coding, "Text");
		type.add(ccType);
		Identifier identi=new Identifier(UseCode.official, "System","value",period ,ccType);
		identifier.add(identi);
		Set<Statushistory> statusHistory = new HashSet<>();
		Statushistory sHis = new Statushistory(Statushistory.StatusCode.onleave,period);
		statusHistory.add(sHis);
		Reference subject = new Reference();
		Reference partOf = new Reference();
		Set<Reference> episodeOfCare= new HashSet<>();
		String uri = ("http://localhost:8080/customer/");
		Reference ref = new Reference("reference",uri,identi,"display");
		Set<Reference> appointment= new HashSet<>();
		appointment.add(ref);
		Set<Diagnosis> diagnoses = new HashSet<>();
		Diagnosis diagnosis1 = new Diagnosis(ref,ccType,2);
		diagnoses.add(diagnosis1);
		Set<Participant> participant = new HashSet<>();
		Participant parti = new Participant(type,period,ref);
		participant.add(parti);

		Encounter e = new Encounter(identifier,Encounter.StatusCode.planned,statusHistory,type,classfromenc,subject,
				episodeOfCare,participant, appointment,period,diagnoses,partOf);
		Encounter retEn = encounterRepository.save(e); //in die DB speichern
		Encounter findEn = encounterRepository.findById(retEn.getId()).get();

		assertTrue(CollectionUtils.isEqualCollection(retEn.getIdentifier(), findEn.getIdentifier()));
		assertTrue(CollectionUtils.isEqualCollection(retEn.getStatusHistory(), findEn.getStatusHistory()));
		assertTrue(CollectionUtils.isEqualCollection(retEn.getType(), findEn.getType()));
		assertEquals(e.getClassfromenc(), findEn.getClassfromenc());
		assertEquals(e.getSubject(), findEn.getSubject());
		assertTrue(CollectionUtils.isEqualCollection(retEn.getEpisodeOfCare(), findEn.getEpisodeOfCare()));
		assertTrue(CollectionUtils.isEqualCollection(retEn.getParticipant(), findEn.getParticipant()));
		assertTrue(CollectionUtils.isEqualCollection(retEn.getAppointment(), findEn.getAppointment()));
		assertEquals(e.getPeriod(), findEn.getPeriod());
		assertTrue(CollectionUtils.isEqualCollection(retEn.getDiagnosis(), findEn.getDiagnosis()));
		assertEquals(e.getPartOf(), findEn.getPartOf());
	}
}