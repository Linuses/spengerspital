package at.spg;

import at.spg.model.*;
import at.spg.repositories.ResearchSubjectRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ResearchSubjectRepositoryTest {

    @Autowired
    ResearchSubjectRepository researchSubjectRepository;

    @Test
    @Transactional
    public void testSaveOneResearchSubject() {
        Set<Identifier> identifier = new HashSet<>();
        Coding classfromenc = new Coding();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
        Set<CodeableConcept> type = new HashSet<>();
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        type.add(ccType);
        Identifier identi=new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
        identifier.add(identi);
        String uri = ("http://localhost:8080/customer/");
        Reference ref = new Reference("reference",uri,identi,"display");
        Set<Reference> basedOn= new HashSet<>();
        basedOn.add(ref);
        DeviceTiming timing = new DeviceTiming(LocalDateTime.of(2000, 01,01,1,1),period);


        ResearchSubject e = new ResearchSubject(identi,ResearchSubject.StatusCode.candit,period,ref,LocalDateTime.of(2000, 01,01,1,1));
        ResearchSubject retEn = researchSubjectRepository.save(e); //in die DB speichern
        ResearchSubject findEn = researchSubjectRepository.findById(retEn.getId()).get();


        assertEquals(retEn.getIdentifier(), findEn.getIdentifier());
        assertEquals(retEn.getStatus(), findEn.getStatus());
        assertEquals(retEn.getPeriod(), findEn.getPeriod());
        assertEquals(retEn.getIndividualization(), findEn.getIndividualization());
        assertEquals(retEn.getRs_datetime(), findEn.getRs_datetime());
    }
}