package at.spg;

import at.spg.model.*;
import at.spg.model.Identifier.UseCode;
import at.spg.repositories.PracRoleRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@SpringBootTest
public class PractitionerRoleRepositoryTest {

	@Autowired //Instanzen auf unsere DB verwalten, dass man nicht unendlich viele DB Connections machen kann (man muss sich selber immer abmelden)
	PracRoleRepository practitionerRoleRepository;

	@Test
	@Transactional
	public void testSaveOnePractitionerRole() {
		// 1. Erstellen einer PractitionerRoleInstanz
		Set<Identifier> identifier = new HashSet<>();
		Coding classfromenc = new Coding();
		Set<Coding> coding = new HashSet<>();
		coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));

		//ich weiß genau was Sie verlangen, bei "In der Speciality sind folgende 2 Codes zu ermöglichen: "
		//reicht das?
		coding.add(new Coding("https://browser.ihtsdotools.org/?perspective=full&conceptId1=394577000","0.1", "Adult mental illness", "408467006", false ));
		coding.add(new Coding("https://browser.ihtsdotools.org/?perspective=full&conceptId1=408467006&edition=MAIN/2021-01-31&release=&languages=en","0.1", "Anesthetics", "394577000", false));
		Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
		Set<CodeableConcept> type = new HashSet<>();
		CodeableConcept ccType = new CodeableConcept(coding, "Text");
		type.add(ccType);
		Identifier identi=new Identifier(UseCode.official, "System","value",period ,ccType);
		identifier.add(identi);
		Set<Statushistory> statusHistory = new HashSet<>();
		Statushistory sHis = new Statushistory(Statushistory.StatusCode.onleave,period);
		statusHistory.add(sHis);
		Reference subject = new Reference();
		Reference partOf = new Reference();
		Set<Reference> episodeOfCare= new HashSet<>();
		String uri = ("http://localhost:8080/customer/");
		Reference ref = new Reference("practitioner",uri,identi,"display");
		Set<Reference> appointment= new HashSet<>();
		appointment.add(ref);
		Set<Diagnosis> diagnoses = new HashSet<>();
		Diagnosis diagnosis1 = new Diagnosis(ref,ccType,2);
		diagnoses.add(diagnosis1);
		Set<Participant> participant = new HashSet<>();
		Participant parti = new Participant(type,period,ref);
		participant.add(parti);




		Set<PractitionerRole_availableTime.DaysOfWeek> daysOfWeeks = new HashSet<>();
		daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.sat);
		daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.tue);
		daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.wed);
		daysOfWeeks.add(PractitionerRole_availableTime.DaysOfWeek.mon);

		Set<PractitionerRole_availableTime> practitionerRoleAvailableTime= new HashSet<>();
		practitionerRoleAvailableTime.add(new PractitionerRole_availableTime(
				daysOfWeeks, true,
				LocalDateTime.of(2000, 01,01,1,1),
				LocalDateTime.of(2000, 01,01,2,2)));


		PractitionerRole e = new PractitionerRole(identifier, true,period,ref,type,practitionerRoleAvailableTime);

		PractitionerRole retEn = practitionerRoleRepository.save(e); //in die DB speichern
		PractitionerRole findEn = practitionerRoleRepository.findById(retEn.getId()).get();

		assertTrue(CollectionUtils.isEqualCollection(retEn.getIdentifier(), findEn.getIdentifier()));
		assertEquals(e.getActive(), findEn.getActive());
		assertEquals(e.getPeriod(), findEn.getPeriod());
		assertEquals(e.getPractitioner(), findEn.getPractitioner());
		assertTrue(CollectionUtils.isEqualCollection(retEn.getSpeciality(), findEn.getSpeciality()));
		assertTrue(CollectionUtils.isEqualCollection(retEn.getAvailableTime(), findEn.getAvailableTime()));
		assertTrue(CollectionUtils.isEqualCollection(retEn.getSpeciality(),findEn.getSpeciality()));
	}
}