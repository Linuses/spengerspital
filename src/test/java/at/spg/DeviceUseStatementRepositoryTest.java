package at.spg;

import at.spg.model.*;
import at.spg.repositories.DeviceUseStatementRepository;
import at.spg.repositories.EncounterRepository;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class DeviceUseStatementRepositoryTest {

    @Autowired
    DeviceUseStatementRepository deviceUseStatementRepository;

    @Test
    @Transactional
    public void testSaveOneDeviceUseStatement() {
        Set<Identifier> identifier = new HashSet<>();
        Coding classfromenc = new Coding();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
        Set<CodeableConcept> type = new HashSet<>();
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        type.add(ccType);
        Identifier identi=new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
        identifier.add(identi);
        String uri = ("http://localhost:8080/customer/");
        Reference ref = new Reference("reference",uri,identi,"display");
        Set<Reference> basedOn= new HashSet<>();
        basedOn.add(ref);
        DeviceTiming timing = new DeviceTiming(LocalDateTime.of(2000, 01,01,1,1),period);


        DeviceUseStatement e = new DeviceUseStatement(identifier,basedOn,DeviceUseStatement.StatusCode.acting,timing,LocalDateTime.of(2000, 01,01,1,1));
        DeviceUseStatement retEn = deviceUseStatementRepository.save(e); //in die DB speichern
        DeviceUseStatement findEn = deviceUseStatementRepository.findById(retEn.getId()).get();

        assertTrue(CollectionUtils.isEqualCollection(retEn.getIdentifier(), findEn.getIdentifier()));
        assertTrue(CollectionUtils.isEqualCollection(retEn.getBasedOn(), findEn.getBasedOn()));
        assertEquals(retEn.getStatus(), findEn.getStatus());
        assertEquals(retEn.getTiming(), findEn.getTiming());
        assertEquals(retEn.getRecordedDate(), findEn.getRecordedDate());
    }
}
