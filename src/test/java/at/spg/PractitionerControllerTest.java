package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.junit.jupiter.api.Test;
import org.springframework.transaction.annotation.Transactional;

import javax.print.attribute.standard.Media;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


@SpringBootTest
@AutoConfigureMockMvc
public class PractitionerControllerTest{
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    public void getAllPractitioner() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/api/practitioner")
                .contentType(MediaType.APPLICATION_JSON))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    public void getAPractitioner() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.get("/api/practitioner/33"))

            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
        System.out.println(MockMvcResultMatchers.header());
    }


    @SneakyThrows
    @Test
    public void deleteAPractitioner() throws Exception {
        mockMvc
            .perform(MockMvcRequestBuilders.delete("/api/practitioner/33"))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    public void postPractitioner() throws Exception { //create
        Practitioner prac = returnFullPractitioner();
        prac.setId(null);
        var json = om.writeValueAsString(prac); //to generate an JSON from the practitioner

        mockMvc
            .perform(MockMvcRequestBuilders.post("/api/practitioner")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                //manuell:
                .content("{\"id\":\"33\"," +
                        "\"identifier\":[{\"id\":\"230\",\"code\":\"official\",\"system\":" +
                        "\"System\",\"value\":\"value\"" +
                        ",\"period\":{\"start\":\"2000-01-01T01:01:00\",\"end\":\"2000-01-01T02:02:00\"}" +
                        "}]," +
                                "\"identifier\":[],"+
                        "\"active\":\"true\",\"gender\":\"other\"" +
                        ",\"birthDate\":\"2000-01-02\"" +
                        "}"))
                //machinell:
//                .content(json))
            .andDo(MockMvcResultHandlers.print())
            .andExpect(MockMvcResultMatchers.status().isCreated());
        System.out.println(json);
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putPractitionerNotFound() throws Exception { //update
        Practitioner e = returnFullPractitioner();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/practitioner/Encounter1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    public void putPractitioner() throws Exception { //update
        Practitioner prac = returnFullPractitioner();
        var json = om.writeValueAsString(prac); //to generate an JSON from the practitioner

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/practitioner/33")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        //manuell:
                        //.content("{\"svnr\": \"33\"}"))
                        //machinell:
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public Practitioner returnFullPractitioner() {
        Set<Identifier> identifier = new HashSet<>();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        Period period = new Period(
                LocalDateTime.of(2000, 01,01,1,1),
                LocalDateTime.of(2000, 01,01,2,2)
        );
        identifier.add(new Identifier(Identifier.UseCode.official, "System","value",period ,ccType));


        Set<HumanName> name = new HashSet<>();
        Set<String> given = new HashSet<>();
        Set<String> prefix = new HashSet<>();
        Set<String> suffix = new HashSet<>();
        name.add(new HumanName("text",HumanName.UseCode.anonymous,given,prefix,suffix,period,"labelfamily"));


        Set<ContactPoint> telecom = new HashSet<>();
        telecom.add(new ContactPoint("Value",ContactPoint.SystemCode.other,ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));

        Set<Address> address = new HashSet<>();
        Set<String> line = new HashSet<>();
        address.add(new Address(Address.UseCode.home,Address.TypeCode.both,"text",line,"vienna",
                "14","vienna","1140","austria",period
        ));

        Set<Attachment> photo = new HashSet<>();
        Long size = 12345L;
        String url ="null";
        photo.add(new Attachment(Attachment.ContentTypeCode.urn,Attachment.LanguageCode.de,"data",
                url,size,"hash",
                "title",LocalDateTime.of(2000,01,01,1,1)));

        Set<Qualification> qualification = new HashSet<>();
        Set<Extension> modifierExtension = new HashSet<>();
        CodeableConcept code = new CodeableConcept();
        qualification.add(new Qualification(modifierExtension,identifier,code,period));

        Set<CodeableConcept> communication = new HashSet<>();
        communication.add(new CodeableConcept(coding,"text"));

        Practitioner prac = new Practitioner(identifier, true, name,telecom,
                address,Practitioner.GenderCode.other,
                LocalDate.of(2000, 01, 01), photo, qualification, communication
        );
        return prac;
    }
}