package at.spg;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;


import org.junit.jupiter.api.Test;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.SetUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import at.spg.model.Address;
import at.spg.model.Attachment;
import at.spg.model.CodeableConcept;
import at.spg.model.Coding;
import at.spg.model.ContactPoint;
import at.spg.model.Extension;
import at.spg.model.HumanName;
import at.spg.model.Identifier;
import at.spg.model.Patient;
import at.spg.model.Patient.GenderCode;
import at.spg.repositories.PatientRepository;
import at.spg.repositories.PractitionerRepository;
import at.spg.model.Identifier.UseCode;
import at.spg.model.Period;
import at.spg.model.Practitioner;
import at.spg.model.Qualification;


@SpringBootTest
public class PractitionerRepositoryTest {
	@Autowired //Instanzen auf unsere DB verwalten, dass man nicht unendlich viele DB Connections machen kann (man muss sich selber immer abmelden)
	PractitionerRepository practitionerRepository;
	
	@Test
	@Transactional
	public void testSaveOnePractioner() {
		// 1. Erstellen einer PractitionerInstanz

		Set<Identifier> identifier = new HashSet<>();
		Set<Coding> coding = new HashSet<>();
		coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
		CodeableConcept ccType = new CodeableConcept(coding, "Text");
		Period period = new Period(
				LocalDateTime.of(2000, 01,01,1,1), 
				LocalDateTime.of(2000, 01,01,2,2)
			);
		Identifier tempide = new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
		tempide.setId(null);
		identifier.add(tempide);

		
		Set<HumanName> name = new HashSet<>();
		Set<String> given = new HashSet<>();
		Set<String> prefix = new HashSet<>();
		Set<String> suffix = new HashSet<>();
		name.add(new HumanName("text",HumanName.UseCode.anonymous,given,prefix,suffix,period,"labelfamily"));
				
		
		Set<ContactPoint> telecom = new HashSet<>();
		telecom.add(new ContactPoint("Value",ContactPoint.SystemCode.other,ContactPoint.UseCode.home,0,
                new Period(LocalDateTime.of(2000,01,01,1,1),
                        LocalDateTime.of(2000,01,01,1,1))));
		
		Set<Address> address = new HashSet<>();
		Set<String> line = new HashSet<>();
		address.add(new Address(Address.UseCode.home,Address.TypeCode.both,"text",line,"vienna",
				"14","vienna","1140","austria",period
				));
		
		Set<Attachment> photo = new HashSet<>();
		Long size = 12345L;
		String url ="null";
		photo.add(new Attachment(Attachment.ContentTypeCode.urn,Attachment.LanguageCode.de,"data",
				url,size,"hash",
				"title",LocalDateTime.of(2000,01,01,1,1)));
		
		Set<Qualification> qualification = new HashSet<>();
		Set<Extension> modifierExtension = new HashSet<>();
		CodeableConcept code = new CodeableConcept();
		qualification.add(new Qualification(modifierExtension,identifier,code,period));
		
		Set<CodeableConcept> communication = new HashSet<>();
		communication.add(new CodeableConcept(coding,"text"));
		
		Practitioner prac = new Practitioner(identifier, true, name,telecom,
				address,Practitioner.GenderCode.other,
				LocalDate.of(2000, 01, 01), photo, qualification, communication
				);		

		Practitioner retPrac = practitionerRepository.save(prac);
		Practitioner findPrac = practitionerRepository.findById(retPrac.getId()).get();

		assertTrue(CollectionUtils.isEqualCollection(retPrac.getIdentifier(), findPrac.getIdentifier()));
		assertEquals(retPrac.isActive(), findPrac.isActive());
		assertTrue(CollectionUtils.isEqualCollection(retPrac.getName(), findPrac.getName()));
		assertTrue(CollectionUtils.isEqualCollection(retPrac.getTelecom(), findPrac.getTelecom()));
		assertTrue(CollectionUtils.isEqualCollection(retPrac.getAddress(), findPrac.getAddress()));
		assertEquals(retPrac.getGender(), findPrac.getGender());
		assertEquals(retPrac.getBirthDate(), findPrac.getBirthDate());
		assertTrue(CollectionUtils.isEqualCollection(retPrac.getPhoto(), findPrac.getPhoto()));
		assertTrue(CollectionUtils.isEqualCollection(retPrac.getQualification(), findPrac.getQualification()));
		assertTrue(CollectionUtils.isEqualCollection(retPrac.getCommunication(), findPrac.getCommunication()));
	}
}
