package at.spg;

import at.spg.model.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@SpringBootTest
@AutoConfigureMockMvc
public class ResearchSubjectControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper om;

    @SneakyThrows
    @Test
    @Transactional
    public void getAllResearchSubject() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/researchSubject")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void getResearchSubject() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/api/researchSubject/rs1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void postResearchSubject() throws Exception { //create
        ResearchSubject e = returnFullresearchSubject();
        e.setId(null);
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.post("/api/researchSubject")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }
    @SneakyThrows
    @Test
    @Transactional
    public void putResearchSubject() throws Exception { //update
        ResearchSubject e = returnFullresearchSubject();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/researchSubject/rs1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void putResearchSubjectNotFound() throws Exception { //update
        ResearchSubject e = returnFullresearchSubject();
        var json = om.writeValueAsString(e); //to generate an JSON from the patient

        mockMvc
                .perform(MockMvcRequestBuilders.put("/api/researchSubject/researchSubject1123123")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                        .content(json))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @SneakyThrows
    @Test
    @Transactional
    public void deleteResearchSubject() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.delete("/api/researchSubject/rs1"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    public ResearchSubject returnFullresearchSubject() {
        Set<Identifier> identifier = new HashSet<>();
        Coding classfromenc = new Coding();
        Set<Coding> coding = new HashSet<>();
        coding.add(new Coding("System","0.1.1", "Code", "<div> ...", false));
        Period period = new Period(LocalDateTime.of(2000, 01,01,1,1), LocalDateTime.of(2000, 01,01,2,2));
        Set<CodeableConcept> type = new HashSet<>();
        CodeableConcept ccType = new CodeableConcept(coding, "Text");
        type.add(ccType);
        Identifier identi=new Identifier(Identifier.UseCode.official, "System","value",period ,ccType);
        identifier.add(identi);
        String uri = ("http://localhost:8080/customer/");
        Reference ref = new Reference("reference",uri,identi,"display");
        Set<Reference> basedOn= new HashSet<>();
        basedOn.add(ref);
        DeviceTiming timing = new DeviceTiming(LocalDateTime.of(2000, 01,01,1,1),period);
        ResearchSubject e = new ResearchSubject(identi,ResearchSubject.StatusCode.candit,period,ref,LocalDateTime.of(2000, 01,01,1,1));
        return e;
    }
}
